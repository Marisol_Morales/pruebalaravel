		<table class="table table-striped table-bordered table-hover"" id="tblCuestionario">
			<thead>
				<tr>
				  	<th>N°</th>
				  	<th>Cuestionario</th>
					<th>Pregunta</th>
					<th align="center">Respuesta Correcta</th>
					<th align="center">Editar</th>
					<th align="center"></th>
				</tr>
			</thead>
			<tbody>
				@foreach($preguntas as $item)
				<tr>
				  <td>{{ $item->idPregunta }}</td>
				  <td>{{ $item->Titulo }}</td>
				  <td>{{ $item->Pregunta }}</td>
				  <td align="center">{{ $item->Resp_Correcta }}</td>
				  <td align="center"> <a href="{{ route('pregunta.show', $item->idPregunta ) }}">  <span class="sub_icon glyphicon glyphicon-file"> </td>
				  <td align="center">
				  {!! Form::open([
			            'method' => 'DELETE',
			            'route' => ['pregunta.destroy', $item->idPregunta]
			       ]) !!}
			       {!! Form::submit('Eliminar', ['class' => 'btn btn-warning']) !!}
			      {!! Form::close() !!} 
			      </td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<div class="form-group">
			<table>
				<thead>
					<tr>
						<th></th>
						<th></th>
					</tr>
					</thead>
					<tbody>
						<tr>
							<td>{{ $preguntas->links() }}</td>
							<td  align="right">
								{{ $preguntas->firstItem() }} de {{ $preguntas->count() }} en total {{ $preguntas->total() }}
							</td>
							<!--
							<td></td>
							<td>
								<div class="text-right">
							  		<a href="{{ route('clasificacionVideo.create', []) }}" class="btn btn-circle blue-steel">Agregar clasificacion <i class="fa fa-plus"></i></a>
							  	</div>
							</td>
							-->
						</tr>
					</tbody>
			</table>
						 
			@if(Session::has('success'))
			   <div class="alert alert-success">
			        {{ Session::get('success') }}
			    </div>
			@endif
		</div>
</div>