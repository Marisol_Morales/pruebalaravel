
@extends('layouts.master')
@section('title', 'BioVideoteca >> Creaciòn de Preguntas')

@section('breadcrumbs')

@endsections

@section('content')

<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-gift"></i>
        Agregar una nueva pregunta al cuestionario
      </div>
  </div>

@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

<div class="portlet-body form">
  <div class="form-body"> 
  <div class="form-horizontal">
    @include('layout/partials/errors')
{!! Form::open(['route' => 'pregunta.store']) !!}
  <input type="hidden" name="_token" value="{{csrf_token() }}">
   {{ Form::hidden('Cuestionario_ID', $CuestionarioID) }}
       <div class="form-group">
          <label class="col-md-3 control-label">Pregunta   </label>
          <div class="col-md-9" >
            {{ Form::text('pregunta', null, ['class' => 'form-control']) }}
          </div>
       </div>

       <div class="form-group">
          <label class="col-md-3 control-label">RespuestaA:</label>
          <div class="col-md-9"" >
             {{ Form::text('respuestaA',  null, ['class' => 'form-control']) }} 
          </div >
       </div>

       <div class="form-group">
          <label class="col-md-3 control-label">RespuestaB:</label>
          <div class="col-md-9" >
             {{ Form::text('respuestaB',  null, ['class' => 'form-control']) }}
          </div>
       </div>
       
       <div class="form-group">
          <label class="col-md-3 control-label">RespuestaC:</label>
          <div class="col-md-9" >
            {{ Form::text('respuestaC',  null, ['class' => 'form-control']) }}
          </div>
        </div>

       <div class="form-group">
          <label class="col-md-3 control-label">Introduzca la letra de la respuesta correcta:</label>
          <div class="col-md-9" >
          {{ Form::text('respuestaCorrecta', null, ['class' => 'form-control']) }}
           </div>
       </div>       

       <div class="form-group">
          <label class="col-md-3 control-label">Explique el motivo de la respuesta:</label>
          <div class="col-md-9" >
          {{ Form::text('solucion', null, ['class' => 'form-control']) }}
           </div>
       </div>
       <div class="form-group">
            {{ Form::submit('Guardar Pregunta',array('class'=>'btn purple')) }}
            {{ Form::reset('Limpiar',array('class'=>'btn btn-success')) }}
       </div>
    </div>
  </div> 
    {!! Form::close() !!}
    <!-- </form> -->
</div>
@endsection

@section('page_script') 
   <script>
    $(document).ready(function(){
        //Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

    });
</script>
@endsection

