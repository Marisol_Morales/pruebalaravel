@extends('layouts.master')
@section('title', 'BioVideoteca >> Listado de Preguntas')
@section('breadcrumbs')
@endsections
@section('content')

<div class="portlet box purple">
    <div class="portlet-title">
	    <div class="caption">
	      <i class="fa fa-gift"></i>Listado de Preguntas
	    </div>
    </div>
 
<div class="portlet-body">
	<div class="panel-body">
					{!! Form::open(['route'=> 'pregunta.index', 'method'=> 'GET', 'class'=> 'navbar-form navbar-left pull-right', 'role'=> 'search']) !!}
						<div class="form-group">
							{!! Form::text('titulo', null, ['class' => 'form-control', 'placeholder' => 'Buscar por cuestionario' ]) !!}
						</div>
						<button type="submit" class="btn btn-default">Buscar</button>
					{!! Form::close() !!}
	</div>
	<div class="table-responsive">

	@if($preguntas->count())
		@include('pregunta.loadPregunta')
	@else
		<h2>No se encontraron preguntadas registradas</h2>
	@endif
	
</div>	
@endsection

@section('page_script')	
	<script>
	$(document).ready(function(){
		
        //Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

		$(document).on('click','.pagination a', function(e){
			e.preventDefault();
			var url = $(this).attr('href');
			getPreguntas(url);
		});
	});

	function getPreguntas(url){

		$.ajax({
			url: url
		}).done(function(data){
			$('.table-responsive').html(data);
			//console.log(data);
			
		}).fail(function (){
			alert('No se pudo leer la informacion');
		});

	}
		
	</script>
@endsection