
@extends('layouts.master')
@section('title', 'BioVideoteca >> Modificar la Pregunta')

@section('breadcrumbs')

@endsections

@section('content')

<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-gift"></i>
        Editar una pregunta
      </div>
  </div>

<div class="portlet-body form">
  <div class="form-body">
    <div class="form-horizontal"> 
    @include('layout/partials/errors')
{{ Form::model($question, array('route' => array('pregunta.update', $question->idPregunta), 'method' => 'PUT')) }}
  <input type="hidden" name="_token" value="{{csrf_token() }}">

      <div class="form-group">
          <label class="col-md-3 control-label">Pregunta:</label>
          <div class="col-md-9" >
            {{ Form::text('pregunta', $question->Pregunta, ['class' => 'form-control']) }} 
          </div>
      </div>

      <div class="form-group">
          <label class="col-md-3 control-label">RespuestaA:</label>
          <div class="col-md-9" >
            {{ Form::text('respuestaA', $question->RespuestaA,  ['class' => 'form-control'] ) }}
          </div class="input-group">
      </div>

      <div class="form-group">
          <label class="col-md-3 control-label">RespuestaB:</label>
          <div class="col-md-9" >
            {{ Form::text('respuestaB', $question->RespuestaB,  ['class' => 'form-control'] ) }} 
          </div>
      </div>

      <div class="form-group">
          <label class="col-md-3 control-label">RespuestaC:</label>
          <div class="col-md-9" >
            {{ Form::text('respuestaC', $question->RespuestaC,  ['class' => 'form-control'] ) }} 
          </div>
      </div>

      <div class="form-group">
          <label class="col-md-3 control-label">Introduzca la letra de la respuesta correcta:</label>
          <div class="col-md-9" >
            {{ Form::text('respuestaCorrecta', $question->Resp_Correcta,  ['class' => 'form-control'] ) }}
          </div>
      </div>
      <div class="form-group">
         {{ Form::submit('Guardar Pregunta',array('class'=>'btn purple')) }}
         {{ Form::reset('Reset',array('class'=>'btn btn-success')) }}
      </div> 
      </div>
    </div>
    {!! Form::close() !!}

</div>
@endsection


@section('page_script') 
   <script>
    $(document).ready(function(){
        //Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

    });
</script>
@endsection
