@extends('layout.master')
@section('title', 'Score Manager >> Error' )

@section('breadcrumbs')
<ul class="page-breadcrumb">
    <li>
        <a href="{{ url('/') }}">Inicio</a>
    </li>
    <li>
        <a href="javascript:history.back(1)">Regresar</a>
    </li>
</ul>
@endsection

@section('content')

<div class="text-center">
  <h1>Lo sentimos, ¡Algo salio mal!</h1>
  <h3>{{$msg}}</h3>
  <p>
    Revisa los datos que ingresaste y las condiciones que te enviaron a esta página, si crees que esto no deberia
    ser comunicate con el responsable del mantenimiento de esta aplicación, de preferencia copia los datos que
    aparecen a contiunuacion cuando reportes este incidente.
  </p>

  <div class="note note-info">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th></th>
          <th rowspan="2">Copia estos datos para reportar</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><strong>msg:</strong></td>
          <td class="text-left">{{ $msg }}</td>
        </tr>
        <tr>
          <td width="5"><strong>URL:</strong></td>
          <td class="text-left">{{request()->url() }}</td>
        </tr>
        <tr>
          <td width="5"><strong>Path:</strong></td>
          <td class="text-left">{{request()->path() }}</td>
        </tr>
        <tr>
          <td><strong>Method:</strong></td>
          <td class="text-left">{{ request()->method() }}</td>
        </tr>
        <tr>
          <td><strong>Input:</strong></td>
          <td class="text-left">
            {{ json_encode(request()->all()) }}
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<div class="text-center">
  <a href="javascript:history.back(1)" class="btn btn-circle btn-primary">Regresar <i class="fa fa-reply"></i></a>
  <a href="{{ url(request()->path())}}" class="btn btn-circle btn-primary">Ir a menu superior <i class="glyphicon glyphicon-arrow-up"></i></a>
</div>
@endsection
