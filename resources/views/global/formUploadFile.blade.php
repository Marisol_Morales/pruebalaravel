      <div class="form-body">

        <!-- Row 1 /-->
        <div class="row" >
          <div class="col-sm-12 col-md-12">
            <div class="form-group">
              <label for="" class="control-label">Archivo:</label>
              <div class="input-group" >
                <span class="input-group-addon input-circle-left"><i class="fa fa-file font-dark" ></i></span>
                <input type="file" name="file" id="file" class="form-control input-circle-right auto" value="{{ old('name', $curso->name) }}" accept=".xls, .ods, .csv, .xlsx">
              </div>
              <span class="help-block">Archivo de datos a cargar</span>
            </div>
          </div>
        </div><!-- /Row 1 -->

        <!-- row 2-->
        <div class="row">
          <div class="form-group">
            <div class="mt-checkbox-list">

              <fieldset id="optgeneral" class="col-md-6">
                <legend class="font-md">Opciones Generales</legend>
                <label for="overwrite" class="mt-checkbox">
                  <input type="checkbox" name="overwrite" id="overwrite"> Reemplazar si ya existe un archivo del mismo nombre
                  <span></span>
                  <div class="help-block small">
                    Si ya se ha cargado un archivo con el mismo nombre en este curso, este sera eliminado
                    y reemplazado con el nuevo (defecto = No).
                  </div>
                </label>
              </fieldset>

              <fieldset id="optdatabase" class="col-md-6 optgroup">
                <legend class="font-md">Opciones para la Base de Datos</legend>
                <label for="incorporarDB" class="mt-checkbox">
                  <input type="checkbox" name="incorporarDB" id="incorporarDB"> Incorporar a la base de Datos
                  <span></span>
                  <div class="help-block small">
                    Luego de cargar el archivo, será analizado e incorporado a la base de datos asignando
                    sus valores a este curso (defecto = No).
                  </div>
                </label>

                <label for="faltantes" class="mt-checkbox">
                  <input type="checkbox" name="faltantes" id="faltantes" checked> Añadir faltantes al catálogo general
                  <span></span>
                  <div class="help-block small font-justify">
                    Si alguno de los alumnos que se agregan no existe en el catalogo general, primero se creara un
                    registro con los datos de este archivo y se relacionara con el registro de este curso (defecto = Sí).
                  </div>
                </label>

                <label for="crearGrupos" class="mt-checkbox">
                  <input type="checkbox" name="crearGrupos" id="crearGrupos" checked> Crear grupos si no existen
                  <span></span>
                  <div class="help-block small font-justify">
                    Si el grupo al que se asigna el alumno no existe se creara un grupo y se añadira, cuidado solo
                    se asignara el numero de grupo el cual sera único para este curso, despues debera editarse
                    manualmente los datos de cada grupo (defecto = Sí).
                  </div>
                </label>

                <label for="reemplazar" class="mt-checkbox">
                  <input type="checkbox" name="reemplazar" id="reemplazar"> Reemplazar valores existentes
                  <span></span>
                  <div class="help-block small">
                    Si ya se han cargado valores para los campos, estos seran eliminados y reemplazados
                    por los valores que existan en este archivo  (defecto = No).
                  </div>
                </label>
              </fieldset>

            </div>
          </div>
        </div>

      </div>

      <div class="form-actions right">
        <a href="{{ url('grupo').'?curso='.$curso->id }}" class="btn btn-circle grey-mint"> Regresar <i class="fa fa-arrow-left" ></i></a>
        <button type="reset" class="btn btn-circle red-flamingo"> Limpiar campos <i class="fa fa-close" ></i></button>
        <button type="submit" class="btn btn-circle green-jungle"> Guardar los cambios <i class="fa fa-check" ></i></button>
      </div>
