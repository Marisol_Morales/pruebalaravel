@if (session()->has('msgInfo'))
<div class="alert alert-info alert-dismissable">
  <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Información:</strong>
  {{ session('msgInfo') }}
</div>
@elseif (session()->has('msgDanger'))
<div class="alert alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
  <strong>Precaución:</strong>
  {{ session('msgDanger') }}
</div>
@endif
