@extends('layouts.master');

@section('title', 'BioVideoteca >> Videos')

@section('breadcrumbs')

@endsection

@section('content')

<div class="col-lg-12 col-xs-8 col-sm-10">
 <!-- BEGIN PORTLET-->
 <div class="portlet box blue">
 <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-gift"></i>USUARIOS CON MAYOR CALIFICACIÒN
    </div>
 </div>
    <div class="portlet light bordered">
        <div class="portlet-body" id="perf_div">
        <!-- inicio de la grafica -->   
            <div class="table-responsive">  
                @if($query->count())  
     
                @include('reportes.loadUsers')

                @else
                    <h2>No se encontro informaciòn</h2>
                @endif
             </div>
         <!-- fin de la grafica-->
        </div>
    </div>	
    
   
</div>
</div>
@endsection

@section('page_script') 
    <script>
    $(document).ready(function(){
        
        //Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

        $(document).on('click','.pagination a', function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            getClasificaciones(url);
        });
    });

    function getClasificaciones(url){
        $.ajax({
            url: url
        }).done(function(data){
            $('.table-responsive').html(data);
        }).fail(function (){
            alert('No se pudo leer la informacion');
        });
    }
        
    </script>
@endsection
