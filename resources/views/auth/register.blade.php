@extends('layouts.app')
@section('title', 'Videoteca Gastroenterologia')

@section('content')

@if(Session::has('success'))
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif

@if(Session::has('message'))
    <div class="alert alert-info">
        {{ Session::get('message') }}
    </div>
@endif

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-2">

            <div class="portlet light bordered">

                <div class="portlet-title">
                     <div class="caption font-green-haze">
                       <i class="icon-user-follow"></i> 
                       <span class="caption-subject bold uppercase">Registro de Usuarios</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>

                <div class="portlet-body form">

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/registro') }}">
                        {{ csrf_field() }}
                     <div class="form-body">

                        <div class="form-group form-md-line-input {{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-2 control-label"><i class="icon-user"></i></label>

                            <div class="col-md-10">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Usuario">
                                <div class="form-control-focus"> </div>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group form-md-line-input {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-2 control-label"><i class="fa fa-envelope-o"></i></label>

                            <div class="col-md-10">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="E-mail">
                                <div class="form-control-focus"></div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group form-md-line-input {{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-2 control-label"><i class="fa fa-key"></i></label>

                            <div class="col-md-10">
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">
                                 <div class="form-control-focus"></div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group form-md-line-input ">
                            <label for="password-confirm" class="col-md-2 control-label"><i class="fa fa-key"></i></label>
                            <div class="col-md-10">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña" required>
                                <div class="form-control-focus"></div>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input ">
                            <label for="lblrole" class="col-md-2 control-label"><i class="fa fa-expeditedssl"></i></label>
                            <div class="col-md-10">
                               {{Form::select('role', array('' => 'Seleccione', 
                                                 'docente' => 'Docente',
                                                 'alumno' => 'Alumno',
                                                  ),
                                                null, ['class' => 'form-control'])
                                }} 
                                <div class="form-control-focus"></div>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input ">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                     </div>
                    </form>
 
 
                </div>


            </div>
          
        </div>
    </div>
</div>

@endsection

@section('page_script')
    <script type="text/javascript">
        
        $(document).ready( function(){
                $('#password').keyup(function(){
                    var _this = $('#password');
                    var pass_1 = $('#password').val();
                            _this.attr('style', 'background:white');
                    if(pass_1.charAt(0) == ' '){
                        _this.attr('style', 'background:#FF4A4A');
                    }
             
                    if(_this.val() == ''){
                        _this.attr('style', 'background:#FF4A4A');
                    }
                });
 
                $('#password-confirm').keyup(function(){
                        var pass_1 = $('#password').val();
                        var pass_2 = $('#password-confirm').val();
                        var _this = $('#password-confirm');
                                _this.attr('style', 'background:white');
                        if(pass_1 != pass_2 && pass_2 != ''){
                            _this.attr('style', 'background:#FF4A4A');
                        }
                });
        });

    </script>
@endsection
