@extends('layouts.app')
@section('title', 'Videoteca Gastroenterologia')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-2">

            <div class="portlet light bordered">
                
                <div class="portlet-title">
                    <div class="caption font-green-haze">
                       <i class="icon-login"></i> 
                       <span class="caption-subject bold uppercase">Inicio de sesiòn</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>

                <div class="portlet-body form">

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                     <div class="form-body">
                         <br>

                  
                         @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                         @endif
                        
                        
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label"><i class="fa fa-envelope-o"></i></label>
                            <div class="col-md-10">
                                {{ Form::text('username', old('username'),  array('placeholder'=>'E-mail', 'class' => 'form-control')) }}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label"><i class="fa fa-key"></i></label>
                            <div class="col-md-10">
                                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Contraseña" name="password"> 
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input">
                               <label class="col-md-2 control-label"><i class="fa fa-expeditedssl"></i></label>
                               <div class="col-md-10">
                                   <select name="roleUser" class="form-control">
                                       <option value="" disabled selected hidden>Tipo de ingreso...</option>
                                       <option value="alumno">Alumno</option>
                                       <option value="docente">Docente</option>
                                       <option value="administrador">Administrador</option> 
                                    </select>
                                     <div class="form-control-focus"> </div>
                               </div>
                               </div>
                        </div>

                        <div class="form-actions">
                            <label class="rememberme mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" name="remember" value="1"> Remember me
                                <span></span>
                                <br>
                                <a href="/index.php/registro">
                                <i class="icon-user"></i> Registro </a>
                            </label>
                            <button type="submit" class="btn green pull-right"> Login </button>
                             <a class="btn btn-link" href="{{ url('') }}">
                                    Forgot Your Password?
                                </a>
                        </div>
                      </div>
                   </form>

                </div>


            </div>
          
        </div>
    </div>
</div>
@endsection
