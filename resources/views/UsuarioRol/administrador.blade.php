@extends('layouts.master')
@section('title', 'BioVideoteca Gastroenterologia y Biomedica')

@section('breadcrumbs')

@endsection

@section('content')
 <!-- BEGIN PORTLET-->
<div class="col-lg-8 col-xs-12 col-sm-12">
 <!-- BEGIN PORTLET-->
    <div class="portlet light bordered">
       <div class="portlet-title">
         <div class="caption">
            <i class="icon-share font-red-sunglo hide"></i>
            <span class="caption-subject font-dark bold uppercase">BIENVENIDO</span>
         </div>
        </div>
        <div class="portlet-body">
             <img src="../assets/layouts/layout6/img/collage.jpeg" align="middle" > 
        </div>
    </div>
</div>
    <!--
       <div class="portlet-title">
         <div class="caption">
            <i class="icon-share font-red-sunglo hide"></i>
            <span class="caption-subject font-dark bold uppercase">Videos mas vistos</span>
         </div>
        </div>

        <div class="portlet-body">
            <div id="site_activities_loading" style="display: none;">
                <img src="../assets/global/img/loading.gif" alt="loading"> </div>
                <div id="site_activities_content" class="display-none" style="display: block;">
                <div id="site_activities" style="height: 228px; padding: 0px; position: relative;"> <canvas class="flot-base" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 480px; height: 228px;" width="480" height="228"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div style="position: absolute; max-width: 48px; top: 209px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 18px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 21px; text-align: center;">DEC</div><div style="position: absolute; max-width: 48px; top: 209px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 18px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 71px; text-align: center;">ENE</div><div style="position: absolute; max-width: 48px; top: 209px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 18px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 119px; text-align: center;">FEB</div><div style="position: absolute; max-width: 48px; top: 209px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 18px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 165px; text-align: center;">MAR</div><div style="position: absolute; max-width: 48px; top: 209px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 18px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 215px; text-align: center;">ABR</div><div style="position: absolute; max-width: 48px; top: 209px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 18px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 262px; text-align: center;">MAYO</div><div style="position: absolute; max-width: 48px; top: 209px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 18px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 313px; text-align: center;">JUN</div><div style="position: absolute; max-width: 48px; top: 209px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 18px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 362px; text-align: center;">JUL</div><div style="position: absolute; max-width: 48px; top: 209px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 18px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 408px; text-align: center;">AGOS</div><div style="position: absolute; max-width: 48px; top: 209px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 18px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 459px; text-align: center;">SEPT</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div style="position: absolute; top: 197px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 14px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 20px; text-align: right;">0</div><div style="position: absolute; top: 149px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 14px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 7px; text-align: right;">500</div><div style="position: absolute; top: 100px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 14px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 1px; text-align: right;">1000</div><div style="position: absolute; top: 52px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 14px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 1px; text-align: right;">1500</div><div style="position: absolute; top: 3px; font-style: normal; font-variant: small-caps; font-weight: 400; font-stretch: normal; font-size: 11px; line-height: 14px; font-family: &quot;Open Sans&quot;, sans-serif; color: rgb(111, 123, 138); left: 1px; text-align: right;">2000</div></div></div><canvas class="flot-overlay" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 480px; height: 228px;" width="480" height="228"></canvas></div>
                    </div>
               
        </div>
        -->

 <div class="col-lg-4 col-xs-12 col-sm-12">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
            <i class="icon-bar-chart font-dark hide"></i>
            <span class="caption-subject font-dark bold uppercase">FACULTAD DE MEDICINA</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="btn-group-img btn-group">
                <img src="../assets/layouts/layout6/img/logo_medicina.png"> 
                <h2>Ubicacion</h2>
                Facultad de Medicina
                Madero y Dr. Aguirre Pequeño
                Col. Mitras Centro sin número
                Monterrey, N.L. México. C.P. 64460
                Conmutador (81) 8329 4154
            </div>
        </div>
        </div>
   </div>
</div>
 <!-- END PORTLET-->
@endsection        
