
<!DOCTYPE html>
@include('layout.partials.metas')
<html lang="en">
     <!-- BEGIN HEAD -->
        @include('layout.partials.head')
         <title><h1>{{ config('app.name', 'Videoteca') }}</h1></title>

        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">

        <!-- Specific Style Sheets -->
        @yield('customCSS')
    <!-- END HEAD -->
    <style>
    .login{
     background-color: #171a1e;
    }
    </style>
    <body class="login">

        <div id="app">
           <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Videoteca') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    
                </div>
            </div>
        </nav>
       
    </div>

        <!-- BEGIN CONTAINER -->
        <div class="container-fluid">
                <div class="page-content-fixed-header">
                    <!-- BEGIN BREADCRUMBS -->
                    @yield('breadcrumbs')
                    <!-- END BREADCRUMBS -->
                    <div class="content-header-menu">
                      {{-- @include('layout.partials.dropdownAjaxMenu') --}}
                    </div>
                </div>
             
                <div class="page-fixed-main-content">
<!-- =================== BEGIN PAGE BASE CONTENT =================== -->
                    @yield('content')
<!-- =================== END PAGE BASE CONTENT =================== -->
                </div>

                <!-- BEGIN FOOTER - ->
              
                <!-- END FOOTER -->
            
        </div>
        <!-- END CONTAINER -->
         @include('layout.partials.foot')
        @yield('page_script')
         <!-- Scripts -->
        <script src="/js/app.js"></script>
    </body>

</html>

