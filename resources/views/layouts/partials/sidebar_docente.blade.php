<div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <div class="page-sidebar navbar-collapse collapse">
                       
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                            <li class="nav-item start ">
                                <img src="../assets/layouts/layout6/img/logo1.png" align="vertical-align: middle;" id="logoVideoteca"> 
                                <a href="/index.php/{{ Auth::user()->role }}" class="nav-link nav-toggle">
                                    <span class="title">Menu principal</span>
                                    <h2 class="uppercase" align="center">{{ Auth::user()->role }} </h2>
                                </a>
                             </li>
                            
                            <li class="heading">
                                <h3 class="uppercase">Operaciones</h3>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-camcorder"></i>
                                    <span class="title">Video</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{ route('video.create', []) }}" class="nav-link ">
                                            <span class="title">Agregar</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="{{ url('docente/video') }}" class="nav-link ">
                                            <span class="title">Examinar</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">Cuestionario</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{ url('docente/cuestionario/create') }}" class="nav-link ">
                                            <span class="title">Agregar</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="{{ url('docente/cuestionario') }}" class="nav-link ">
                                            <span class="title">Examinar</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-note"></i>
                                    <span class="title">Pregunta</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{ url('docente/pregunta' ) }}" class="nav-link ">
                                            <span class="title">Examinar</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="layout_blank_page.html" class="nav-link ">
                                    <i class="icon-layers"></i>
                                    <span class="title">Clasificaciones</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{ route('clasificacionVideo.create', 'docente') }}" class="nav-link ">
                                            <span class="title">Agregar</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="{{ route('clasificacionVideo.index', 'docente') }}" class="nav-link ">
                                            <span class="title">Examinar</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
</div>
