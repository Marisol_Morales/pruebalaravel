<!DOCTYPE html>
@include('layout.partials.metas')
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!-- BEGIN HEAD -->
        @include('layout.partials.head')
        <!-- Specific Style Sheets -->
        @yield('customCSS')
    <!-- END HEAD -->

    <body class="">
        <!-- BEGIN HEADER ADMINISTRADOR -->
        @include('layouts.partials.header')
        <!-- END HEAD -->
        <!-- BEGIN CONTAINER -->
        <div class="container-fluid">
            <div class="page-content page-content-popup">
                <div class="page-content-fixed-header">
                    <!-- BEGIN BREADCRUMBS -->
                    @yield('breadcrumbs')
                    <!-- END BREADCRUMBS -->
                    <div class="content-header-menu">
                      {{-- @include('layout.partials.dropdownAjaxMenu') --}}
                    </div>
                </div>
                @include('layouts.partials.sidebar')
                <div class="page-fixed-main-content">
<!-- =================== BEGIN PAGE BASE CONTENT =================== -->
                    @yield('content')
<!-- =================== END PAGE BASE CONTENT =================== -->
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->
         @include('layout.partials.foot')
        @yield('page_script')
    </body>
   
</html>
