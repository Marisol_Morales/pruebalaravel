<!DOCTYPE html>
@include('layout.partials.metas')
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
        @include('layout.partials.head')
        <!-- Specific Style Sheets -->
        @yield('customCSS')
    <!-- END HEAD -->

    <body class="">
        @include('layout.partials.header')

        <!-- BEGIN CONTAINER -->
        <div class="container-fluid">
            <div class="page-content page-content-popup">
                <div class="page-content-fixed-header">
                    <!-- BEGIN BREADCRUMBS -->
                    @yield('breadcrumbs')
                    <!-- END BREADCRUMBS -->
                    <div class="content-header-menu">
                      {{-- @include('layout.partials.dropdownAjaxMenu') --}}
                    </div>
                </div>

                @if (Auth::guest())   
                     @include('layout.partials.sidebar')
                @else
                    @if(Auth::user()->role === 'alumno')
                        @include('layouts.partials.sidebar_alumno')
                    @elseif(Auth::user()->role === 'docente')
                        @include('layouts.partials.sidebar_docente')
                    @elseif(Auth::user()->role === 'administrador')
                        @include('layouts.partials.sidebar')
                    @else
                        @include('layout.partials.sidebar')
                    @endif
                @endif

                <div class="page-fixed-main-content">

<!-- =================== BEGIN PAGE BASE CONTENT =================== -->
                    @yield('content')
<!-- =================== END PAGE BASE CONTENT =================== -->
                </div>
             
            </div>
        </div>
        <!-- END CONTAINER -->
      
        @include('layout.partials.foot')
        @include('layout.partials.footer')
        @yield('page_script')
        
    </body>

</html>
