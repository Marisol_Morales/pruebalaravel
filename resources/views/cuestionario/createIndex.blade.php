@extends('layouts.master')
@section('title', 'BioVideoteca >> Creacion de Preguntas')

@section('breadcrumbs')

@endsection

@section('content')

<div class="portlet box blue">
  <div class="portlet-title">
  <div class="caption">
  	 Agregar un nuevo Cuestionario
  </div>
  </div>
</div>

<div class="portlet-body form">
     
<div class="form-body">
   <div class="form-horizontal">
@include('layout/partials/errors')
{!! Form::open(['route' => 'cuestionario.store']) !!}
     <div class="form-group">
	     <label class="col-md-3 control-label">Titulo del cuestionario</label> 
	     <div class="col-md-9" >
			    {{ Form::text('titulo', null,  ['class' => 'form-control']) }}
       </div>
      </div>

     <div class="form-group">
			<label class="col-md-3 control-label">Listado de videos</label>
			 <div class="col-md-9" >
			     {{ Form::select('videos', $videolist, null, array('class' => 'form-control')) }}
        </div>
	    </div>
     
      <div class="form-group">
	      <label class="col-md-3 control-label">Clasificacion del Cuestionario</label>
			  <div class="col-md-9" >
            	{{ Form::select('clasificaciones', $clasificaciones, null, array('class' => 'form-control')) }}
        </div>
		  </div>
       <!-- 
      <div class="form-group">
     
      <label class="col-md-3 control-label">Descripcion del Cuestionario</label>
        <div class="col-md-9">
	        {{ Form::textArea('descripcion', null,  ['class' => 'form-control']) }}
	      </div>
      </div>
      -->
      
      <div class="form-group">
          <label class="col-md-3 control-label">Activo </label> 
        <div class="col-md-9">
          {{ Form::checkbox('activo', 0, null, ['class' => 'field']) }}
        </div>
      </div>
      
      <div class="form-group">
        <div class="col-xs-12 col-md-12 form-group">
	         <button class="btn btn-primary pull-right" type="reset">Limpiar</button>
	         <button class="btn btn-primary pull-right" type="submit">Guardar</button>
        </div>
      </div>
      </div>
</div> 
{!! Form::close() !!}
</div>
@endsection                 

@section('page_script') 
   <script>
    $(document).ready(function(){
        //Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

    });
</script>
@endsection
