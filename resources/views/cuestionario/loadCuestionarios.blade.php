<table class="table table-striped table-bordered table-hover" id="tblCuestionario">
    <thead>
	<tr>
	  <th>N°</th>
	  <th>Cuestionario</th>
	 <!-- <th align="center">Descripcion</th> -->
	  <th align="center"> Status </th>
    @if(Auth::user()->role != 'alumno')
	  <th align="center"> Editar </th>
	  <th></th>
    <th></th>
    @endif
	</tr>
  </thead>
  <tbody>
@foreach($cuestionarios as $cuestionario)
   <tr>
   <td>{{ $cuestionario->idCuestionario }}</td>
   <td>{{ $cuestionario->Titulo }}</td>
  <!-- <td>{{ $cuestionario->Descripcion }}</td>-->
   <td align="center">
   @if($cuestionario->activo == 1)
    activo
   @else
    inactivo
   @endif
   </td>
  @if(Auth::user()->role != 'alumno')
   <td align="center"> <a href="{{ route('cuestionario.show', $cuestionario->idCuestionario ) }}">  <span class="sub_icon glyphicon glyphicon-file">
   </td>
  <td align="center">
     {!! Form::open([
            'method' => 'DELETE',
            'route' => ['cuestionario.destroy', $cuestionario->idCuestionario]
       ]) !!}
       {!! Form::submit('Eliminar', ['class' => 'btn purple' ]) !!}
      {!! Form::close() !!} 
   </td>
   <td align="center">
     <a href="{{ route('pregunta.create', 'idCuestionario='.$cuestionario->idCuestionario ) }}" class="btn btn-sm green">Pregunta<i class="fa fa-plus"></i>
   </td>
   @endif
	</tr>
@endforeach
  </tbody>
  </table>
  <div class="form-group">
          <table>
            <thead>
              <tr>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{{ $cuestionarios->links() }}</td>
                <td  align="right">{{ $cuestionarios->firstItem() }} de {{ $cuestionarios->count() }} en total {{ $cuestionarios->total() }}</td>
              </tr>
            </tbody>
          </table>
           
          @if(Session::has('success'))
              <div class="alert alert-success">
                  {{ Session::get('success') }}
              </div>
          @endif
  </div>

 </div>