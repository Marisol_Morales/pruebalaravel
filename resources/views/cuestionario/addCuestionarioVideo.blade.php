
@extends('layouts.master');

@section('title', 'BioVideoteca >> Video-Cuestionario')

@section('breadcrumbs')

@endsection

@section('content')
 
<!-- TABLA PARA FILTRAR CUESTIONARIOS -->
<div class="portlet-body">
 	<div class="table-responsive">
 	    <div class="portlet box purple">
	 	    <div class="portlet-title">
				<div class="caption">
	                <i class="fa fa-globe"></i>Listado de videos para agregar Cuestionarios 
				</div>
				<div class="tools"> </div>
	        </div>
	        <div class="portlet-body">
	        	<div class="panel-body">
					{!! Form::open(['route'=> 'video.index', 'method'=> 'GET', 'class'=> 'navbar-form navbar-left pull-right', 'role'=> 'search']) !!}
						<div class="form-group">
							{!! Form::text('titulo', null, ['class' => 'form-control', 'placeholder' => 'Buscar por Titulo' ]) !!}
							{!! Form::select('list_clasificacion', $clasificaciones, null, ['class' => 'form-control', 'placeholder' => 'seleccione']) !!}
						</div>
						<button type="submit" class="btn btn-default">Buscar</button>
					{!! Form::close() !!}
				</div>
				
				<table class="table table-striped table-bordered table-hover" id="tblVideo">
					<thead>
					<tr>
						<th>Nº</th>
						<th>Titulo</th>
						<th align="center">Descripcion</th>
						<th align="center">Duracion</th>
						<th align="center">Acciones</th>
					    
					</tr>
				  </thead>
				  	<tbody>
				  	@foreach($videos as $item)
				  	<tr>
						<td>{{ $item->idVideo }}</td>
						<td>{{ $item->titulo }}</td>
					   	<td>{{ $item->descripcion }}</td>
					  	<td>{{ $item->duracion}}</td>
					  	<td>
					   		<a href="{{ route('videoCuestionario.show', $item->idVideo) }}" class="btn btn-warning"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>

					   	</td>
					   	
					</tr>
					@endforeach
					</tbody>
				</table>

				<div class="form-group">
					<table>
						<thead>
							<tr>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{ $videos->links() }}</td>
								<td  align="right">{{ $videos->firstItem() }} de {{ $videos->count() }} en total {{ $videos->total() }}</td>
							</tr>
						</tbody>
					</table>
					 
				
				</div>
			</div>
		</div>
	</div>
</div>
<!-- FIN DE LA TABLA -->

@endsection

@section('page_script') 
   <script>
    $(document).ready(function(){
        //Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

    });
</script>
@endsection

