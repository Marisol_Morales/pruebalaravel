@extends('layouts.master')
@section('title', 'BioVideoteca Gastroenterologia y Biomedica')

@section('breadcrumbs')

@endsection

@section('content')
<div class="col-lg-8 col-xs-12 col-sm-12">
 <!-- BEGIN PORTLET-->
    <div class="portlet light bordered">
       <div class="portlet-title">
         <div class="caption">
            <span class="caption-subject font-dark bold uppercase">VIDEOTECA</span>
         </div>
        </div>
        <div class="portlet-body">
            <div class="btn-group-img btn-group">
                <img src="../assets/layouts/layout6/img/Fondo1.jpeg"> 
            </div>
        </div>
    </div>

 
 </div>
 <div class="col-lg-4 col-xs-12 col-sm-12">
 <!-- BEGIN PORTLET-->
    <div class="portlet light bordered">
       <div class="portlet-title">
         <div class="caption">
            <i class="icon-share font-red-sunglo hide"></i>
            <span class="caption-subject font-dark bold uppercase">FACULTAD DE MEDICINA</span>
         </div>
        </div>
        <div class="portlet-body">
            <div class="btn-group-img btn-group">
                <img src="../assets/layouts/layout6/img/logo_medicina.png"> 
                <h2>Ubicacion</h2>
                Facultad de Medicina
                Madero y Dr. Aguirre Pequeño
                Col. Mitras Centro sin número
                Monterrey, N.L. México. C.P. 64460
                Conmutador (81) 8329 4154
            </div>
        </div>
    </div>

 
 </div>
 <!-- END PORTLET-->
@endsection        
