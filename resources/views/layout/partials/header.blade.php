        <!-- BEGIN HEADER -->
        <header class="page-header">
            <nav class="navbar" role="navigation">
                <div class="container-fluid">
                    <div class="havbar-header">
                        <!-- BEGIN LOGO -->

                        <a id="index" class="navbar-brand" href="/index.php"> <h1><span class="font-yellow-crusta" >VIDEOTECA DE BIOMEDICA y GASTROENTEROLOGIA, UANL</h1>
                        </a>
                         <!-- END LOGO -->
                        <!-- BEGIN TOPBAR ACTIONS -->
                        <div class="topbar-actions">

                            <!-- BEGIN HEADER SEARCH BOX -->
                         @if (Auth::guest())   
                            <div class="btn-group-img btn-group">
                                <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                  <img src="../assets/layouts/layout6/img/login2.png" alt=""> 
                                </button>
                                <ul class="dropdown-menu-v2" role="menu">
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="/index.php/registro">
                                            <i class="icon-user-follow"></i>
                                            Registrarse 
                                        </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                       <a href="/index.php/login">
                                        <i class="icon-key"></i> Iniciar sesion </a>
                                    </li>
                                </ul>
                                
                            </div>
                            <!-- END GROUP NOTIFICATION -->

                            <!-- BEGIN USER AUTHENTICATION -->
                        @else 
                           <div class="btn-group-img btn-group">
                                <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <label class="font-yellow-crusta"> <h3>{{ Auth::user()->name }}</h3> </label>
                                <!--  <img src="../assets/layouts/layout6/img/avatar1.jpg" alt=""> -->
                                </button>
                                <ul class="dropdown-menu-v2" role="menu">
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="/index.php/logout">
                                            <i class="icon-lock"></i> Cerrar Sesion </a>
                                    </li>
                                </ul>
                            </div>
                        @endif
                            <!-- END USER PROFILE -->
                        </div>
                        <!-- END TOPBAR ACTIONS -->
                    </div>
                </div>
                <!--/container-->
            </nav>
        </header>
        <!-- END HEADER -->
