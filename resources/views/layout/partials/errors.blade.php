@if($errors->count())
		<div class="alert alert-danger">
		  <p><strong>¡Atención!</strong> Se han presentado errores en el formulario</p>
		  <ul>
		  @foreach($errors->all() as $error)
		    <li>{{ $error }}</li>
		  @endforeach
		  </ul>
		</div>
@endif

@if(Session::has('success'))
    <div class="alert alert-success">
      {{ Session::get('success') }}
    </div>
@endif