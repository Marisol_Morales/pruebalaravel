<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="/css/app.css" rel="stylesheet">
 </head>

 <body class="container">

  <div class="row">
    <div class="page-sidebar-wrapper">
      <ul>
        <li>Esta es la barra de navegacion</li>
      </ul>
    </div>
  </div>

<!--
<div class="row" >
  <!-- Barra de navegacion superior -- >
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
     <div class="container">
      <div class="navbar-header">
       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
       </button>
       <a class="navbar-brand" href="#">Administrador de Calificaciones</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
       <ul class="nav navbar-nav">
        <li class="active"><a href="#">Inicio</a></li>
        <li><a href="#about">Cursos</a></li>
        <li><a href="#contact">Grupos</a></li>
        <li><a href="#contact">Alumnos</a></li>
        <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menú Expandible <span class="caret"></span></a>
         <ul class="dropdown-menu">
          <li><a href="#">Opcion 1</a></li>
          <li><a href="#">Opción 2</a></li>
          <li><a href="#">Opción 3</a></li>
          <li role="separator" class="divider"></li>
<!--
          <li class="dropdown-header">Nav header</li>
-- >
          <li><a href="#">Opciones</a></li>
          <li><a href="#">Preferencias</a></li>
         </ul>
        </li>
       </ul>
      </div><!--/.nav-collapse -- >
     </div>
    </nav>
</div>
-->

      <div class="row" style="margin-top:60px">
        <div class="col-lg-4 col-md-4">
          @section('sidebar')
          This is the master sidebar.
          @show
        </div>

        <div class="col-lg-8 col-md-8">
        @yield('content')
        </div>
      </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/app.js"></script>
 </body>
</html>
