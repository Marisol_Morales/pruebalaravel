@extends('app')
@section('content')
{!! Form::open(['route' => 'clasificacionVideo.store']) !!}
	<p> Campos</p>

{!! Form::button('Guardar', ['type' => 'submit', 'class' => 'btn btn-primary' !!]) }
	
{!! Form::close() !!}
<table>
	<thead>
		<tr>
		 <th>Clasificacion</th>
		 <th>Nombre</th>
		 <th>Descripcion</th>
		 <th align="center">Acciones</th>
		</tr>
	</thead>
	<tbody>
@foreach($clasificaciones as $pregunta)
	<tr>
	<td>{{ $pregunta->idClasificacion }}</td>
		<td>{{ $pregunta->Nombre }}</td>
		<td align="center">{{ $pregunta->Descripcion }}</td>
		<td class="text-center">
		  <button type="submit" class="btn btn-danger btn-xs">
		  <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
		  </button>
		  <a href="" class="btn btn-info btn-xs">
		  <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
		  </a>
		</td>
	</tr>
@endforeach
	</tbody>
</table>
@endsection		  
