
@extends('layouts.master')
@section('title', 'BioVideoteca >> Listado de Clasificaciones')

@section('breadcrumbs')

@endsections

@section('content')

<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-gift"></i>Listado de Clasificaciones
    </div>
  </div>
 
 <div class="portlet-body">
			  
	<div class="table-responsive">

	@if($clasificaciones->count())	
 
   	@include('otros.loadClasificacion')

	@else
		<h2>No se encontraron clasificaciones registradas</h2>
	@endif
	
	</div>
@endsection
@section('page_script')	
	<script>
	$(document).ready(function(){
		
        //Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

		$(document).on('click','.pagination a', function(e){
			e.preventDefault();
			var url = $(this).attr('href');
			getClasificaciones(url);
		});
	});

	function getClasificaciones(url){
		$.ajax({
			url: url
		}).done(function(data){
			$('.table-responsive').html(data);
			console.log(data);
			
		}).fail(function (){
			alert('No se pudo leer la informacion');
		});
	}
		
	</script>
@endsection
