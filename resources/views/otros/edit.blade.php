@extends('layouts.master')
@section('title', 'BioVideoteca >> Editar una clasificacion')

@section('breadcrumbs')

@endsections

@section('content')
 <div class="portlet box blue">
   <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-gift"></i>
      Editar una clasificacion de Video
    </div>
  </div>

 <div class="portlet-body form">

	<div class="form-body"> 
	  <div class="form-horizontal"> 
	  @include('layout/partials/errors')
{{ Form::model($clasificacion, array('route' => array('clasificacionVideo.update', $clasificacion->idClasificacion), 'method' => 'PUT')) }}
    	<div class="form-group">
		 	<label class="col-md-3 control-label">Nombre </label>
			<div class="col-md-9" >
			  {{ Form::text('nombre', $clasificacion->Nombre, ['class' => 'form-control']) }}
		    </div>
		</div>	

		<div class="form-group">
		    <label class="col-md-3 control-label">Descripcion de la clasificacion </label>
		    <div class="col-md-9" >
		    {{ Form::textarea('descripcion', $clasificacion->Descripcion, ['class' => 'form-control']) }}
		   </div>
		</div>		
		    {{ Form::submit('Guardar Categoria', array('class'=>'btn purple')) }}
		    {{ Form::reset('Limpiar',array('class'=>'btn btn-success')) }}
		</div>
	   </div> 
 </div> 
{{ Form::close() }}
</div>

@endsection

@section('page_script') 
   <script>
    $(document).ready(function(){
        //Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

    });
</script>
@endsection
