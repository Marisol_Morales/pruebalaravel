<table class="table table-striped table-bordered table-hover" id="idTblClasificacion">
			<thead>
			<tr>
			<th class="text-center">Clasificacion</th>
				<th class="text-center">Nombre</th>
				<th class="text-center">Descripcion</th>
				<th class="text-center">Acciones</th>
				<th></th>
			</tr>
			</thead>
		<tbody>
		@foreach($clasificaciones as $pregunta)
			<tr>
			<td class="text-center">{{ $pregunta->idClasificacion }}</td>
			<td class="text-center">{{ $pregunta->Nombre }}</td>
			<td >{{ $pregunta->Descripcion }}</td>
			<td class="text-center">
			 <form action=""method="POST" class="form" style="floating:left">
				<a href="{{ route('clasificacionVideo.show', $pregunta->idClasificacion ) }}" class="btn btn-info btn-xs">
				<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
				</a>
			</form>
			</td>
			<td>
				{!! Form::open([
		            'method' => 'DELETE',
		            'route' => ['clasificacionVideo.destroy', $pregunta->idClasificacion]
	          ])
	      !!}
		       	<button type="submit" class="btn btn-danger btn-xs">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
		        {!! Form::close() !!} 
			</td>
				</tr>
		@endforeach
		</tbody>
									
</table>
	{{ $clasificaciones->links() }}
</div>	