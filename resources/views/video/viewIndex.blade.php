@if(Session::has('success'))
					    <div class="alert alert-success">
					        {{ Session::get('success') }}
					    </div>
					@endif
<table class="table table-striped table-bordered table-hover" id="tblVideo">
					<thead>
					<tr>
						<th>Nº</th>
						<th>Titulo</th>
						<th align="center">Descripcion</th>
						<th align="center">Duracion</th>
						<th align="center">Clasificacion</th>
					    @if(Auth::user()->role != 'alumno')
						<th align="center">Editar</th>
						@endif
						@if(Auth::user()->role === 'administrador')
					    <th>Eliminar</th>
					    @endif
					    <th>Ver</th>
					    @if(Auth::user()->role === 'administrador')
					    <th>Download</th>
					    @endif
					</tr>
				  </thead>
				  	<tbody>
				  	@foreach($videos as $item)
				  	<tr>
						<td>{{ $item->idVideo }}</td>
					   	<td>{{ $item->titulo }}</td>
					   	<td>{{ $item->descripcion }}</td>
					  	<td>{{ $item->duracion}}</td>
					  	<td>{{ $item->Nombre}}</td>
					  	@if(Auth::user()->role != 'alumno')
					   	<td>
					   		<a href="{{ route('videoCuestionario.show', $item->idVideo) }}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
					   	</td>
					   	@endif
					   	@if(Auth::user()->role === 'administrador')
					   	<td>
					   		<a href="{{ route('video.destroy', $item->idVideo) }}" class="btn btn-danger"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
					   	</td>
					   	@endif
					   	<td><a href="{{ route('video.show', $item->idVideo) }}" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
					   	@if(Auth::user()->role === 'administrador')
					   	<td><a href="{{ url('video/download', $item->idVideo) }}" class="btn btn-success"><span class="glyphicon glyphicon-save" aria-hidden="true"></span></a></td>
					   	@endif
					</tr>
					@endforeach
					</tbody>
				</table>
				<div class="form-group">
					<table>
						<thead>
							<tr>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{ $videos->links() }}</td>
								<td  align="right">{{ $videos->firstItem() }} de {{ $videos->count() }} en total {{ $videos->total() }}</td>
							</tr>
						</tbody>
					</table>
					
</div>