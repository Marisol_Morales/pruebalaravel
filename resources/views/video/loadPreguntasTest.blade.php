                  <div class="table-scrollable">
                    
                    <table class="table table-striped table-hover">
                        <thead>
                          <tr>
                              <th> Pregunta </th>
                              <th> Respuestas</th>
                              <th></th>
                              <th></th>
                              <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($question as $item)
                            <tr data-id="{{ $item->idPregunta }}">
                                <td>{{ $item->Pregunta }} </td>
                                <td>
                                  <div class="form-group">
                                    <div class="col-md-6">
                                      <span>A) {{ $item->RespuestaA }}</span>
                                    </div>
                                  </div>
                                </td>
                                <td>
                                  <div class="form-group">
                                    <div class="col-md-6">
                                      <span>B) {{ $item->RespuestaB }}</span>
                                    </div>
                                  </div>
                                </td>
                                <td>
                                  <div class="form-group">
                                    <div class="col-md-6">
                                      <span>C) {{ $item->RespuestaC }}</span>
                                    </div>
                                  </div>
                                </td>
                                <td style="width:20px">
                                  <form id="form-{{ $item->idPregunta }}">
                                      @include('video.formRespuestaAlumno')
                                  </form>               
                                  <div class="response-Send-{{ $item->idPregunta }}">
                                  </div>
                                 </div>
                                </td>
                            </tr>
                          @endforeach
                        </tbody>
                    </table>
                  
                  </div>
                  <div class="form-group">
                  <!--PAGINADO DE LA TABLA -->
                    <table>
                      <thead>
                        <tr>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>

                        <tr>
                          <td>{{ $question->links() }}</td>
                          <td  align="right">{{ $question->firstItem() }} de {{ $question->count() }} en total {{ $question->total() }} preguntas</td>
                        </tr>

                      </tbody>
                    </table>
                <!-- TERMINO DEL PAGINADO -->
           
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                  </div>