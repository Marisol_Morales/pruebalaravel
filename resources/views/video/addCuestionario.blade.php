@extends('layouts.master');

@section('title', 'BioVideoteca >> Videos')

@section('breadcrumbs')

@endsection

@section('content')
 @if(Session::has('success'))
        <h2>{!! Session::get('success') !!}</h2>
@endif

<div class="col-lg-6 col-xs-12 col-sm-12">
 <!-- BEGIN PORTLET-->
    <div class="portlet light bordered">
       <div class="portlet-title">
         <div class="caption">
            <span class="caption-subject font-dark bold uppercase">VIDEO</span>
        </div>

        </div>
        <div class="portlet-body">
           <video id="laracasts-video_html5_api" class="vjs-tech" preload="auto" height="340" width="450" controls autoplay>
              <source src="{{ $linkVideo }}" type="video/mp4">
              <source src="{{ $linkVideoOgg }}" type="video/ogg">
           </video>
           <div class="caption">
            <span class="caption-subject font-dark bold uppercase"> {{ $videoDetail->descripcion }} </span>
           </div>
        </div>
    </div>
 </div>
 <div class="col-lg-6 col-xs-12 col-sm-12">
 <!-- BEGIN PORTLET-->
    <div class="portlet light bordered">
       <div class="portlet-title">
         <div class="caption">
            <i class="icon-share font-red-sunglo hide"></i>
            <span class="caption-subject font-dark bold uppercase">Video Editar</span>
         </div>
        </div>
        <div class="portlet-body">
           
            {{ Form::open(array('route' => 'videoCuestionario.store' )) }}
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/> 
        
        <p>
           
            {{ Form::label('Editar el titulo del Video') }}
            {{ Form::text('nombre',$videoDetail->titulo,array('class'=>'form-control')) }}
            <input type="hidden" name="idVideo" value="{{ $idVideoVal }}"/> 
           
        </p>
        
        <p>
            {{ Form::label('Cuestionàrio') }}
            {{ Form::select('lstCuestionarios', $listaCuestionario, null, array('disabled' => 'disabled','class' => 'form-control')) }}
        </p>    

        <p>
            {{ Form::submit('Agregar', array('class'=>'btn btn-primary')) }}

            <button type="button" class="btn btn-default"><a href="/index.php/{{ Auth::user()->role }}">Salir</a></button>
        </p>
    {{ Form::close() }}
         
        </div>
    </div>

 
 </div>


@endsection

@section('page_script') 
   <script>
    $(document).ready(function(){
        //Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

    });
</script>
@endsection
