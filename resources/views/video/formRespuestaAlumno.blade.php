	<input type="hidden" name="respuestaCorrecta" id="respuestaCorrecta" value="{{ $item->Resp_Correcta }}">
    
    <input type="hidden" name="idPregunta" id="idPregunta" value="{{ $item->idPregunta }}">
    
    @if($item->respuesta)
    <input type="text" name="respAlumno" id="respAlumno-{{ $item->idPregunta }}" value="{{ $item->respuesta }}" class="form-control" size="3" disabled="disabled">
    @else
     <input type="text" name="respAlumno" id="respAlumno-{{ $item->idPregunta }}" value="{{ $item->respuesta }}" class="form-control" size="3">
      <a href="" class="send-respuesta" style="color:red">Guardar respuesta</a>
    @endif
    <input type="hidden" id="token" value="{{ csrf_token() }}">
    
    <input type="hidden" name="idCuestionario" id="idCuestionario" value="{{ $item->idCuestionario }}">
    
    <input type="hidden" name="hidden_addRespTest" id="hidden_addRespTest" value="{{url('/preguntaRespuesta/store')}}">
    
    <input type="hidden" name="hiddenRol" id="hiddenRol" value="{{Auth::user()->role}}">
    
    <input type="hidden" name="solucion" id="solucion" value="{{ $item->solucion }}" class="form-control" visible= false>
    
    
