@extends('layouts.master');

@section('title', 'BioVideoteca >> Videos')

@section('breadcrumbs')

@endsection

@section('content')

<div class="col-lg-8 col-xs-8 col-sm-10">
 <!-- BEGIN PORTLET-->
    <div class="portlet light bordered">
       <div class="portlet-title">
         <div class="caption">
            <span class="caption-subject font-dark bold uppercase">VIDEO</span>
            @if(Session::has('success'))
              <h4>{!! Session::get('success') !!}</h4>
            @endif
         </div>
        </div>
        <div class="portlet-body">
         
          <video id="videohtml5" class="vjs-tech" preload="auto" height="400" width="600" controls>
              <source src="{{ $linkVideo }}" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' >
              <source src="{{ $linkVideo }}" type='video/ogg; codecs="theora, vorbis"' >
              Video no soportado. Descargalo <a href="{{ $linkVideo }}">aqui</a>
          </video>
         
          {!! Form::open(['route' => 'videoCalificacion.store']) !!}
             <div class="col-md-8">
                {{ Form::hidden('videoID', $detalleVideo->idVideo) }}
                {{ Form::select('cal_Video', $evaluarVideo, null, array('class' => 'form-control')) }}
              </div>
              <Button type ="submit">Calificar</Button>
          {!! Form::close() !!}
          </div>
          
    </div>

</div>

<div class="col-lg-4 col-xs-8 col-sm-10">
   <div class="portlet light bordered">
      <div class="portlet-title">
         <div class="caption">
            <span class="caption-subject font-dark bold uppercase">DESCRIPCION</span>
         </div>
      </div>
      {{ $detalleVideo->descripcion }}
      <br>
      <br>
      Nota: Algunos videos tienen cuestionarios, favor de responderlos.
  </div>
</div>
  <!-- INICIA EL FORMULARIO DE LAS PREGUNTAS-->
 <div class="col-lg-12 col-xs-12 col-sm-12">

    <div class="portlet light bordered">
        <div class="portlet-title">
         <div class="caption">
            <i class="icon-share font-red-sunglo hide"></i>
            <span class="caption-subject font-dark bold uppercase">Cuestionario</span>
            <br>
            Instrucciones: Responda el siguiente cuestionario agregando la "letra" de la respuesta correcta.
         </div>
        </div>

        <div class="portlet-body">
       
          @if($question->count())
             <div class="portlet box purple">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-comments">{{ $tituloCuestionario->Titulo }}</i> </div>
                </div>

              <div class="portlet-body">
                <div class="table-responsive">  
                    @include('video.loadPreguntasTest')
                </div>
              <p>
                {{ Form::hidden('url',$linkVideo,array('class'=>'form-control')) }} 
              </p>
                   <form id="form-saveTest">
                  
                  <input type="hidden" name="numVideo" id="numVideo" value="{{ $detalleVideo->idVideo }}">
                  
                  <input type="hidden" name="numCuestionario" id="numCuestionario" value="{{ $tituloCuestionario->idCuestionario }}"> 
                 
                  <input type="hidden" name="userLoggin" id="userLoggin" value="{{ Auth::id() }}">

                  <button id="Btn-endTest">Finalizar Cuestionario</button>
                  @if(Session::has('successTest'))
                    <div class="alert alert-success">
                        {{ Session::get('successTest') }}
                    </div>
                  @endif
                </form>
                <div id="CalificacionFinal">
                </div>
                <div id="solucionTest"></div>
               <!-- {{ Form::close() }} -->
              </div>
            </div>
              
           @else
               <h3>El video no tiene un cuestionario asignado</h3>
           @endif
<!-- TERMINA LA SECCION DE PREGUNTAS -->
        </div>
    </div>

@endsection

@section('page_script') 
  <script>

  $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $(document).ready(function(){

      //Actualiza el logo de la aplicacion de Videoteca
      var currentPage = window.location.href.split('/');
      var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
      var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
      $('#logoVideoteca').attr('src', urlLogo);
       
      $(document).on('click','.pagination a', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        getVideos(url);
      });
      
      //Accion del boton que guarda las respuestas
      $('.send-respuesta').click(function(e){    
          e.preventDefault(); 
          var id = $(this).parents('tr').data('id')
          //Valida que el campo tenga informacion antes de enviar
          var resInput = $('#respAlumno-' + id).val();
          if( resInput === ""){
            alert('Falta introducir la respuesta');
            return false;
          }
          else
          {
            //alert('save');
            var form = $("#form-" + id);
            var rol = $('#hiddenRol').val();
            var link = $('#hidden_addRespTest').val();
            var url = link.replace('preguntaRespuesta/store', rol + '/preguntaRespuesta');
            var data = form.serialize();
             $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                            "_token" : $('#token').val(),
                            "idPregunta" : id,
                            "idCuestionario" : $('#idCuestionario').val(),
                            "respuestaCorrecta" : $("#respuestaCorrecta").val(),
                            "respAlumno" : $('#respAlumno-' + id).val()
                       },
                    dataType: "json",
                    success: function( data, textStatus, jQxhr ){
                        alert(data.msg);
                        $('#respAlumno-' + id).val(data.respuestaAlumno);
                        $('#respAlumno-' + id).prop('disabled',true);
                        form.prop('disabled', true);
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    }
                });
          }
      }); 

      //Accion del boton que termina el cuestionario
      $("#Btn-endTest").click(function(e){
          e.preventDefault();
          var rol = $('#hiddenRol').val();
          var link = $('#hidden_addRespTest').val();
          var url = link.replace('preguntaRespuesta/store', rol + '/usuarioTest');
          var data = $("#form-saveTest").serialize();
          //alert(url + "\n" + data);

           $.ajax({
                  type: 'POST',
                  url: url,
                  data: {
                           "_token" : $('#token').val(),
                           "numVideo" : $("#numVideo").val(),
                           "numCuestionario" : $("#numCuestionario").val(),
                           "userLoggin": $("#userLoggin").val()
                         },
                  dataType: "json",
                  success: function( data, textStatus, jQxhr ){
                      alert(data.msg);
                      var jsonP =$.parseJSON(data.correctas);
                       $('#CalificacionFinal').html('<h3><font color="blue">' + 'Total de respuestas correctas: ' 
                        + data.totalRespuestasCorrectas + ' Calificacion Obtenida: '+ data.Calificacion+'</font></h3><br><h4><font color="green"> Respuestas:</font></h4>');
                       for(var i=0; i<jsonP.length;++i){
                            $("#solucionTest").append('<h5><font color="green">'+ jsonP[i].Pregunta  +"   ["+ jsonP[i].Resp_Correcta +"]      Explicacion:" + jsonP[i].solucion + '</font></h5>');
                          }  
                       
                  },
                  error: function( jqXhr, textStatus, errorThrown ){
                      console.log( errorThrown );
                  }
              });

      });
      

  });

  function getVideos(url){

    $.ajax({
      url: url
    }).done(function(data){
      $('.table-responsive').html(data);
    }).fail(function (){
      alert('No se pudo leer la informacion');
    });

  }

</script>
@endsection