@extends('layouts.master');

@section('title', 'BioVideoteca >> Videos')

@section('breadcrumbs')

@endsection

@section('content')
<div class="portlet-body">
 	<div class="table-responsive">
 	    <div class="portlet box purple">
	 	    <div class="portlet-title">
				<div class="caption">
	                <i class="fa fa-globe"></i>Listado de Videos 
				</div>
				<div class="tools"> </div>
	        </div>
	        <div class="portlet-body">
	        	<div class="panel-body">
					{!! Form::open(['route'=> 'video.index', 'method'=> 'GET', 'class'=> 'navbar-form navbar-left pull-right', 'role'=> 'search']) !!}
						<div class="form-group">
							{!! Form::text('titulo', null, ['class' => 'form-control', 'placeholder' => 'Buscar por Titulo' ]) !!}
							{!! Form::select('list_clasificacion', $clasificaciones, null, ['class' => 'form-control', 'placeholder' => 'seleccione']) !!}
						</div>
						<button type="submit" class="btn btn-default">Buscar</button>
					{!! Form::close() !!}
				</div>
				<div class="table-responsive2">
					@include('video.viewIndex')
				</div>
			</div>
		</div>
	</div>
	
</div>
@endsection

@section('page_script')	
	<script>
	$(document).ready(function(){
		//Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

		$(document).on('click','.pagination a', function(e){
			e.preventDefault();
			var url = $(this).attr('href');
			getVideos(url);
		});
	});

	function getVideos(url){
		$.ajax({
			url: url
		}).done(function(data){
			$('.table-responsive2').html(data);
			//console.log(data);
			
		}).fail(function (){
			alert('No se pudo leer la informacion');
		});
	}
		
	</script>
@endsection