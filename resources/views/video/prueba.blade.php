@extends('layouts.master')
@section('title', 'BioVideoteca >> Cargar videos')

@section('breadcrumbs')

@endsections

@section('content')

<div class="portlet box blue">
 <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-gift"></i>
      Agregar un nuevo Video
    </div>
  </div>

<div class="portlet-body form">
  <div class="form-body">
    <div class="form-horizontal">
    
@if(Session::has('success'))
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif

{{ Form::open(array('route' => 'video.store', 'files'=>true )) }}
         {!! Form::hidden('_token', csrf_token()) !!}
        <div class="form-group">
           <label class="col-md-3 control-label">Titulo</label>
           <div class="col-md-9">
            {{ Form::text('titulo',null,array('class'=>'form-control')) }}
           </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Descripcion del video</label>
            <div class="col-md-9">
            {{ Form::text('descripcion',null,array('class'=>'form-control')) }}
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Video seleccione</label>
            <div class="col-md-9">
            {{ Form::file('archivo','',array('id'=>'','class'=>'')) }}
            </div>
        </div>

        <div class="form-group">
          <label class="col-md-3 control-label">Clasificacion del Video</label>
          <div class="col-md-9" >
                {{ Form::select('Listaclasificacion', $clasificaciones, null, array('class' => 'form-control')) }}
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-9" >
            {{ Form::submit('Agregar',array('class'=>'btn btn-primary')) }}
            {{ Form::reset('Limpiar',array('class'=>'btn btn-success')) }}
          </div>  
        </div>
{{ Form::close() }}
        </div>
      </div>
    </div>
</div>

@endsection

@section('page_script') 
   <script>
    $(document).ready(function(){
        //Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

    });
</script>
@endsection
