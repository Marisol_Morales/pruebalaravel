@extends('layouts.master');

@section('title', 'BioVideoteca >> Videos')

@section('breadcrumbs')

@endsection

@section('content')

<div class="col-lg-8 col-xs-8 col-sm-10">
 <!-- BEGIN PORTLET-->
    <div class="portlet light bordered">
       <div class="portlet-title">
         <div class="caption">
            <span class="caption-subject font-dark bold uppercase">VIDEO</span>
            @if(Session::has('success'))
              <h4>{!! Session::get('success') !!}</h4>
            @endif
         </div>
        </div>
        <div class="portlet-body">
         
          <video id="videohtml5" class="vjs-tech" preload="auto" height="400" width="600" controls>
              <source src="{{ $linkVideo }}" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' >
              <source src="{{ $linkVideo }}" type='video/ogg; codecs="theora, vorbis"' >
              Video no soportado. Descargalo <a href="{{ $linkVideo }}">aqui</a>
          </video>
         
          {!! Form::open(['route' => 'videoCalificacion.store']) !!}
             <div class="col-md-8">
                {{ Form::hidden('videoID', $detalleVideo->idVideo) }}
                {{ Form::select('cal_Video', $evaluarVideo, null, array('class' => 'form-control')) }}
              </div>
              <Button type ="submit">Calificar</Button>
          {!! Form::close() !!}
           </div>
         
    </div>

</div>

<div class="col-lg-4 col-xs-8 col-sm-10">
   <div class="portlet light bordered">
      <div class="portlet-title">
         <div class="caption">
            <span class="caption-subject font-dark bold uppercase">DESCRIPCION</span>
         </div>
      </div>
      {{ $detalleVideo->descripcion }}
      <br>
      <br>
      Nota: Algunos videos tienen cuestionarios, favor de responderlos.
  </div>
</div>
<div class="col-lg-12 col-xs-12 col-sm-12">

    <div class="portlet light bordered">
        <div class="portlet-title">
         <div class="caption">
            <i class="icon-share font-red-sunglo hide"></i>
            <span class="caption-subject font-dark bold uppercase">Cuestionario</span>
            <br>
            Instrucciones: Responde el siguiente cuestionario agregando la "letra" de la respuesta correcta.
         </div>
        </div>

        <div class="portlet-body">
               <h3>Cuestionario evaluado</h3>
        </div>
    </div>

@endsection

@section('page_script') 
  <script>
 $(document).ready(function(){

      //Actualiza el logo de la aplicacion de Videoteca
      var currentPage = window.location.href.split('/');
      var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
      var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
      $('#logoVideoteca').attr('src', urlLogo);
  });
</script>
@endsection