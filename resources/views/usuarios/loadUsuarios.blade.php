<table class="table table-striped table-bordered table-hover" id="idTblUsers">
			<thead>
			<tr style="color:chocolate">
				<th class="text-center" style="font-size: 20px;">Nº</th>
					<th class="text-center" style="font-size: 20px;">Nombre</th>
					<th class="text-center" style="font-size: 20px;">E-mail</th>
					<th class="text-center" style="font-size: 20px;">Rol de Usuario</th>
					<th class="text-center" style="font-size: 20px;">Editar</th>
					<th></th>
				</tr>
			</thead>
		<tbody>
		@foreach($listUser as $item)
			<tr>
			<td class="text-center">{{ $item->id }}</td>
			<td class="text-center">{{ $item->name }}</td>
			<td class="text-center">{{ $item->email }}</td>
			<td class="text-center">{{ $item->role }}</td>
			<td class="text-center">
			 <form action=""method="POST" class="form" style="floating:left">
				<a href="{{ route('user.show', $item->id ) }}" class="btn btn-info btn-xs">
				<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
				</a>
			</form>
			</td>
			<td>
				{!! Form::open([
		            'method' => 'DELETE',
		            'route' => ['user.destroy', $item->id]
	          ])
	      !!}
		       	<button type="submit" class="btn btn-danger btn-xs">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
		        {!! Form::close() !!} 
			</td>
				</tr>
		@endforeach
		</tbody>
									
</table>
	{{ $listUser->links() }}
</div>	