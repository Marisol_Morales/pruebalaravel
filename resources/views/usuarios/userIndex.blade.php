@extends('layouts.master')
@section('title', 'BioVideoteca >> Listado de Clasificaciones')

@section('breadcrumbs')

@endsections

@section('content')
<div class="portlet box blue">
 <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-gift"></i>REPORTE DE USUARIOS
    </div>
 </div>
 
 <div class="portlet-body">
	 <div class="table-responsive">
		@if($listUser->count())	
 
	   		@include('usuarios.loadUsuarios')

		@else
			<h2>No se encontraron usuarios registrados</h2>
		@endif	
	 </div>
 </div>
</div>
@endsection


@section('page_script')	
	<script>
	$(document).ready(function(){
		
       //Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

		$(document).on('click','.pagination a', function(e){
			e.preventDefault();
			var url = $(this).attr('href');
			getUsers(url);
		});
	});

	function getUsers(url){
		$.ajax({
			url: url
		}).done(function(data){
			$('.table-responsive').html(data);
			//console.log(data);
			
		}).fail(function (){
			alert('No se pudo leer la informacion');
		});
	}
		
	</script>
@endsection