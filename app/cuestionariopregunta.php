<?php

namespace Biobiblioteca;

use Illuminate\Database\Eloquent\Model;

class cuestionariopregunta extends Model
{
    //Tabla que se va asociar al modelo
     protected $fillable = ['idCuestionario', 'idPregunta'];

	 protected $table = 'cuestionario_pregunta';

	 protected $primaryKey = 'idCuestionario';
	 
	 public $timestamps = true;
}
