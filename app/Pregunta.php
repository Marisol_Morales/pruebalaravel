<?php

namespace Biobiblioteca;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pregunta extends Model
{
   
     //Tabla que se va asociar al modelo
	 protected $table = 'preguntas';

     protected $fillable = ['Pregunta', 'RespuestaA','RespuestaB','RespuestaC','Resp_Correcta', 'solucion'];

     protected $primaryKey = 'idPregunta';

     public $timestamps = true;

     public function scopeCuestionario($query, $titulo = ""){

    	//dd('scope: '.$titulo);
      	if(trim($titulo) != ""  ){
    		$query= DB::table('cuestionario_pregunta')
                ->join('preguntas', 'preguntas.idPregunta', '=', 'cuestionario_pregunta.idPregunta')
                ->join('cuestionarios', 'cuestionarios.idCuestionario', '=', 'cuestionario_pregunta.idCuestionario')
                ->select('cuestionario_pregunta.idPregunta','cuestionario_pregunta.idCuestionario','cuestionarios.Titulo','preguntas.Pregunta','preguntas.Resp_Correcta')
                ->where('cuestionarios.titulo', "like" , '%'.$titulo.'%')
                ->orderBy('cuestionario_pregunta.idCuestionario', 'ASC');
     	}else
        {
            $query= DB::table('cuestionario_pregunta')
                ->join('preguntas', 'preguntas.idPregunta', '=', 'cuestionario_pregunta.idPregunta')
                ->join('cuestionarios', 'cuestionarios.idCuestionario', '=', 'cuestionario_pregunta.idCuestionario')
                ->select('cuestionario_pregunta.idPregunta','cuestionario_pregunta.idCuestionario','cuestionarios.Titulo','preguntas.Pregunta','preguntas.Resp_Correcta')
                ->orderBy('cuestionario_pregunta.idCuestionario', 'ASC');
               
        }

        return $query;
    }
}
