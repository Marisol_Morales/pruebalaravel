<?php

namespace Biobiblioteca;

use Illuminate\Database\Eloquent\Model;

class cuestionarioVideo extends Model
{
    //Tabla que se va asociar al modelo
     protected $fillable = ['idCuestionario','idVideo'];

	 protected $table = 'cuestionario_Video';

	 protected $primaryKey = 'idVideo';
	 
	 public $timestamps = true;

}
