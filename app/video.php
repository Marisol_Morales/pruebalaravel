<?php

namespace Biobiblioteca;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class video extends Model
{
    //create consulta para tabla de videos
    protected $table = 'videos';

    protected $fillable = ['url', 'titulo', 'ancho', 'alto', 'framerate', 'duracion', 'clasificacion', 'descripcion'];

    protected $primaryKey = 'idVideo';

    public $timestamps = true;

    
    public function scopeTitulo($query, $titulo = ""){

    	//dd('scope: '.$titulo);
    	if(trim($titulo) != ""  ){
    		$query= DB::table('videos')
                ->join('clasificaciones', 'videos.clasificacion', '=', 'clasificaciones.idClasificacion')
                ->select('videos.idVideo','videos.titulo','videos.descripcion','videos.duracion', 'clasificaciones.Nombre')
                ->where('videos.titulo', "like" , '%'.$titulo.'%')
                ->orWhere('videos.descripcion', "like", '%'.$titulo.'%')
                ->orderBy('idVideo', 'ASC');
     	}else
        {
            $query= DB::table('videos')
                    ->join('clasificaciones', 'videos.clasificacion', '=', 'clasificaciones.idClasificacion')
                    ->select('videos.idVideo','videos.titulo','videos.descripcion','videos.duracion', 'clasificaciones.Nombre')
                    ->orderBy('videos.idVideo', 'ASC');
               
        }

        return $query;
    }

    public function scopeClasificacion($query, $idClasificacion){

        $Filtro_Clasificacion = $idClasificacion;
        if($idClasificacion != 0){
            $query = DB::table('clasificaciones')
                            ->where('clasificaciones.idClasificacion',$idClasificacion);
        }
    }
    
}
