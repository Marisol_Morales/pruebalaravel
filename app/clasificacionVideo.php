<?php

namespace Biobiblioteca;
use Illuminate\Database\Eloquent\Model;


class clasificacionVideo extends Model
{
    //Tabla que se va asociar al modelo
     protected $fillable = ['Nombre', 'Descripcion'];

	 protected $table = 'clasificaciones';
	 
	 public static function getValidationRules(){
	 	return [
	 		'idClasificacion' => ['required', 'max:5'],
	 		'Nombre'          => ['required', 'max:150'],
	 		'Descripcion'     => ['required', 'max:400'],
	 	];
	 }

	 protected $primaryKey = 'idClasificacion';


	 public $timestamps = true;
}
