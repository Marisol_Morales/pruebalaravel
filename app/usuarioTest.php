<?php

namespace Biobiblioteca;

use Illuminate\Database\Eloquent\Model;

class usuarioTest extends Model
{
    //
    protected $table = 'usuarioTest';

    protected $fillable = ['idUsuario', 'idCuestionario', 'idVideo', 'idCalificacionObtenida'];

    protected $primaryKey = 'idUsuario';

    public $timestamps = true;

}
