<?php

namespace Biobiblioteca;

use Illuminate\Database\Eloquent\Model;

class cuestionario extends Model
{
    //Configuracion de la tabla para almacenar los datos
	 protected $table = 'cuestionarios';

	 protected $fillable = ['Titulo', 'idClasificacion', 'Descripcion', 'activo'];

	 protected $primaryKey = 'idCuestionario';

   public $timestamps = true;



   public function scopeTitulo($query, $titulo = ""){

      	if(trim($titulo) != ""  ){
    		  $query->where('titulo', "like" , '%'.$titulo.'%');
       	}
        else{
          $query->orderBy('idCuestionario', 'asc');
        }
        return $query;
    }


    public function scopeStatus($query, $idStatus){

    	//dd('scope: '.$idStatus);
      if($idStatus === '1' || $idStatus === '0'){
    		$query->where('activo', $idStatus);
     	}
     
      return $query;
    }
}
