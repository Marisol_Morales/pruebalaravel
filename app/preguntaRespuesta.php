<?php

namespace Biobiblioteca;

use Illuminate\Database\Eloquent\Model;

class preguntaRespuesta extends Model
{
    //
    protected $table = 'pregunta_respuesta';

    protected $fillable = ['idPregunta', 'respuesta', 'respuestaCorrecta', 'idCuestionario', 'idUsuario'];

    protected $primaryKey = 'idPregunta';

    public $timestamps = true;
}
