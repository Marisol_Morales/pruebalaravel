<?php

namespace Biobiblioteca;

use Illuminate\Database\Eloquent\Model;

class calificacionVideo extends Model
{
    //
     protected $table = 'calificacionVideo';

     public $timestamps = true;
}
