<?php

namespace Biobiblioteca;

use Illuminate\Database\Eloquent\Model;

class videoCalificacion extends Model
{
    //
     protected $table = 'videoCalificacion';

     protected $fillable = ['idVideo', 'idCalificacion_Video'];

     protected $primaryKey = 'idVideo';

     public $timestamps = true;
}
