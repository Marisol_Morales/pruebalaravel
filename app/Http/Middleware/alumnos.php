<?php

namespace Biobiblioteca\Http\Middleware;

use Closure;
use Redirect;
use Session;
use Illuminate\Contracts\Auth\Guard;

class alumnos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    private $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
       
        if ( !$this->auth->user()->role === 'docente' )
        {
            $this->auth->logout();

            if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
                return redirect()->to('login');
            }
        }

        
        return $next($request);
    }
}
