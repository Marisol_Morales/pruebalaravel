<?php

namespace Biobiblioteca\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Auth\SessionGuard;

class administrador
{
   protected $auth;

   public function __construct(Guard $auth)
   {
        $this->auth = $auth;
   }

    public function handle($request, Closure $next)
    {
        //dd($request);
        //Si el usuario ya esta logueado solo lo direcciona
        /*
        if($this->auth->user()->role === 'docente')
        {
            return redirect()->to('docente');
        }
        else if($this->auth->user->role === 'alumno'){
            return redirect()->to('docente');
        }
        else{
            redirect()->to('login');
        }
        */

        return $next($request);
    }
}
