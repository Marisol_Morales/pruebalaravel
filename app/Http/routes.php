<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});

Route::get('clasificacion_option', function(){
	
});

Route::resource('/pregunta', 'preguntaController');

Route::resource('video', 'videoController');

Route::resource('/cuestionario', 'cuestionarioController');


//Route::post('/clasificacionVideo/store','clasificacionVideoController@store')
Route::resource('/clasificacionVideo', 'clasificacionVideoController');

Route::post('/upload', function(){
     if(Input::hasFile('archivo')) {
          Input::file('archivo')
               ->move('carpetarArchivos','NuevoNombre');
     }
     return Redirect::back(('/');
});
*/
//Route::get('/registro', 'Auth\AuthController@getRegister');
//Route::post('/registro', 'Auth\AuthController@postRegister');