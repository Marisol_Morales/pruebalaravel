<?php
namespace Biobiblioteca\Http\Controllers;
use Illuminate\Http\Request;
use Biobiblioteca\Http\Requests;
use Biobiblioteca\clasificacionVideo;
use Illuminate\Support\Facades\Input;


class clasificacionVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
		//Se hace un llamado al modelo
	   $clasificaciones = clasificacionVideo::orderBy('idClasificacion','ASC')
                                            ->paginate(10);
        if( $request->ajax() ){
            return view('otros.loadClasificacion',['clasificaciones' => $clasificaciones])->render();    
        }
       //Se devuelven los datos en una variable mostrada en la vista
		return view('otros.viewIndex',['clasificaciones' => $clasificaciones]);
		
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Crear clasificaciones
		return view('otros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'nombre' => ['required','max:60'],
        'descripcion' => ['required', 'max:400']
      ]);

      //Modelo que recopila la informacion que sera enviada la base de datos
      $entry = new clasificacionVideo();
      $entry->Nombre = Input::get('nombre');
      $entry->Descripcion = Input::get('descripcion');
      $entry->save();
      $user = \Auth::user()->role;

      return redirect()->to($user.'/clasificacionVideo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idClasificacion)
    {
        //Se busca el valor de la clasificacion
		$findClasificacion = clasificacionVideo::find($idClasificacion);

         return view('otros.edit' , ['clasificacion' => $findClasificacion] );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idClasificacion)
    {
        //Busca la clasificacion a modificar
		 $findClasificacion = clasificacionVideo::find($idClasificacion);
         
         $this->validate($request,[
                'nombre'=> ['required', 'max:100'],
                'descripcion' => ['required', 'max:400'],
                ]);

         //Guarda la informacion modificada
         $findClasificacion->nombre = $request->input('nombre');
         $findClasificacion->descripcion  = $request->input('descripcion');
         $findClasificacion->save();

         //Regresamos a la vista del listado de clasificaciones
        $user = \Auth::user()->role;
        return redirect()->to($user.'/clasificacionVideo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idClasificacion)
    {
        //Buscar y eliminar el registro seleccionado
		clasificacionVideo::destroy($idClasificacion);
        $user = \Auth::user()->role;
        return redirect()->to($user.'/clasificacionVideo');
        /*
        return view('otros.viewIndex', 
                                     ['clasificaciones' => clasificacionVideo::all()
                     ]);
        */
    }
}
