<?php

namespace Biobiblioteca\Http\Controllers;

use Biobiblioteca\video;
use Biobiblioteca\cuestionario;
use Biobiblioteca\Http\Requests;
use Biobiblioteca\clasificacionVideo;
use Biobiblioteca\calificacionVideo;
use Biobiblioteca\usuarioTest;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

//use Auth;
use FFmpegMovie;

class videoController extends Controller
{
    
    public function index(Request $request)
    {
       
        $video = video::titulo($request->get('titulo'))
                        ->paginate(10);

        $clasificaciones = clasificacionVideo::pluck('Nombre','idClasificacion');
        
        if($request->ajax() ){
          return view ('video.viewIndex', [ 'videos' => $video]);
        }

        return view ('video.listadovideo', [ 'videos' => $video, 'clasificaciones' =>  $clasificaciones]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $clasificaciones = clasificacionVideo::pluck('Nombre','idClasificacion');
        return view('video.prueba',  [ 'clasificaciones' => $clasificaciones]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
         
        $this->validate($request,[
            'archivo'=> 'required',
            'Listaclasificacion' => 'required',
        ]);

        //obtenemos el campo file definido en el formulario
        $file = Input::file('archivo');
        $nombre = Input::file('archivo')->getClientOriginalName();
         
       
        //Se guarda el video en el disco seleccionado
        $disk = \Storage::disk('uploads');  
         
         $nameFileSave =  time().'_'.$nombre;   
         $disk->put( $nameFileSave, \File::get($file),'public');
          
       // $fileRecover = public_path()."/videos/".$nameFileSave;

      if($disk->exists($nameFileSave))
        {
           //Recuperar el archivo del disco almacenado
          $filePath = $disk-> getDriver()-> getAdapter()-> getPathPrefix();
          
          $movie = new FFmpegMovie($filePath.$nameFileSave);
          $duracion = $movie-> getDuration();
          $frames = $movie-> getFrameCount();
          $width = $movie-> getFrameWidth();
          $height = $movie-> getFrameHeight();

          //Modelo que recopila la informacion que sera enviada la base de datos
          $entry = new video([
             'url' => $filePath.$nameFileSave,
             'titulo' => Input::get('titulo'),
             'ancho' => $width,
             'alto' => $height,
             'framerate' => $frames,
             'duracion' => $duracion,
             'clasificacion' => Input::get('Listaclasificacion'),
             'descripcion' => Input::get('descripcion')
           ]);
           $entry->save();

           $authRol= \Auth::user()->role;
         
          //Devuelve el resultado a la misma pagina 
          Session::flash('success', 'Archivo Subido Correctamente '.$nombre);
          return redirect()->to($authRol.'/video');
  
        }
        else
        {
          return "No se encontro : ".$nameFileSave;
        }
         
    }


    /**
     * Display the specified resource.
     */
    public function show(Request $request,$idVideo)
    {
        //Extrae la URL y la particiona para buscar en la ubicacion correcta el video
        $path = $request->url();
        //$array_Path = explode('/', "http://ibhu.noip.me:6080/index.php/administrador/usuarioTest");
        $array_Path = explode('/', $path);
        $URL_Path = $array_Path[0].'//'.$array_Path[2];
        
        $urlPublic = $URL_Path; //'http://pruebalaravel';

        $videoFind = video::find($idVideo);
        
        $RecuperarUrl = DB::table('videos')
                                ->select('url')
                                ->where('idVideo',$idVideo)
                                ->first();

        $values = $RecuperarUrl->{'url'}; 
        
        $nameExt = explode('/', $values );

        $UrlFile = '/'.$nameExt[6].'/'.$nameExt[7];

        $calificarVideo = calificacionVideo::pluck('calificacion','idCalificacion_Video');

        $linkVideomp4 = $urlPublic.$UrlFile;
        //$linkVideoOgg = str_replace('mp4','ogg',$linkVideomp4);
        //Se valida que el usuario no haya finalizado el cuestionario
        $idUser = \Auth::id();

        $findTest = DB::table('usuarioTest')
                            ->select(DB::raw('count(usuarioTest.idUsuario) as totRegistros, usuarioTest.idCuestionario'))
                            ->where('usuarioTest.idUsuario', $idUser)
                            ->where('usuarioTest.idVideo', $idVideo)
                            ->get();
        
        $cuestionarioRespondidoTot = (int) $findTest[0]->totRegistros;
        //dd($findTest);
        //Se busca en la tabla de resultados si el usuario ya ha respondido el cuestionario
        if( $cuestionarioRespondidoTot > 0){
          //Cuando ya ha sido evaluado unicamente muestra el video
          return view('video.loadVideo',['detalleVideo' => $videoFind, 
                                          'linkVideo' => $urlPublic.$UrlFile,
                                           'evaluarVideo' => $calificarVideo,
                                        ]);
         
        }else{
          
          /*
          CONSULTA FINAL - ACTUALIZAR HOY 07 04 2017
            SELECT cp.idCuestionario, cp.idPregunta, p.Pregunta, p.RespuestaA, p.RespuestaB, p.RespuestaC, p.Resp_Correcta, pr.respuesta 
            From cuestionario_pregunta cp 
            INNER JOIN preguntas p on p.idPregunta = cp.idPregunta 
            LEFT JOIN pregunta_respuesta pr on pr.idPregunta = p.idPregunta and pr.idUsuario = 3 
            LEFT JOIN cuestionario_Video cv on cv.idCuestionario = cp.idCuestionario and cv.idVideo = 2
            WHERE cp.idCuestionario = 1
          */
          $idCuestionario = \DB::table('cuestionario_Video as cv')
                                ->select('cv.idCuestionario')
                                ->where('cv.idVideo', $idVideo)
                                ->first();

          if($idCuestionario !== null){

            $questions = \DB::table('cuestionario_pregunta AS cp')
                             ->join('preguntas AS p', 'p.idPregunta', '=', 'cp.idPregunta')
                             ->leftJoin('pregunta_respuesta AS pr', function ( $join ) use ($idUser) {
                                        $join->on('pr.idPregunta', '=', 'p.idPregunta');
                                        $join->on('pr.idUsuario', DB::raw($idUser));
                             })
                             ->leftJoin('cuestionario_Video AS cv', function( $join ) use ($idVideo) {
                                        $join->on('cv.idCuestionario', '=', 'cp.idCuestionario');
                                        $join->on('cv.idVideo', DB::raw($idVideo));
                             })
                             ->select('cp.idCuestionario', 'cp.idPregunta','p.Pregunta','p.RespuestaA','p.RespuestaB','p.RespuestaC','p.Resp_Correcta','pr.respuesta','p.solucion')
                             ->where('cp.idCuestionario',$idCuestionario->idCuestionario)
                             ->paginate(15);

            $titCuestionario = \DB::table('cuestionarios as c')
                            ->select('c.Titulo','c.idCuestionario')
                             ->where('c.idCuestionario',$idCuestionario->idCuestionario)
                            ->first();
            
            if($request->ajax() ){
              //dd('aqui estoy');
              return  view('video.loadPreguntasTest', ['question' => $questions ]);
            }
          
            return view('video.playVideo', ['detalleVideo' => $videoFind, 
                                            'linkVideo' => $urlPublic.$UrlFile,
                                            //'linkVideoOgg' => $linkVideoOgg,
                                            'evaluarVideo' => $calificarVideo,
                                            'question' => $questions,
                                            'tituloCuestionario' => $titCuestionario
                                            ] );
          }
          else{
            return redirect()->back()->with('success', 'El video no tiene un cuestionario seleccionado');
          }

        }
        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request,$idVideo)
    {

         //Extrae la URL y la particiona para buscar en la ubicacion correcta el video
        $path = $request->url();
        $array_Path = explode('/', $path);
        $URL_Path = $array_Path[0].'//'.$array_Path[2];


        //Busca el video en la base de datos
        $videoFind = video::find($idVideo);
        $RecuperarUrl = DB::table('videos')
                                ->select('url')
                                ->where('idVideo',$idVideo)
                                ->first();

        $values = $RecuperarUrl->{'url'}; 
        
        $nameExt = explode('/', $values );

        $urlVideo = Storage::url( $nameExt[8] );
        //~ dd($urlVideo);


        //$contents = Storage::disk('uploads')->setVisibility( $nameExt[8], 'public' );
        $urlVideo_Ogg = str_replace('mp4', 'ogg', $urlVideo);

        $urlPublic = $URL_Path; //'http://pruebalaravel';
        $calificarVideo = calificacionVideo::pluck('calificacion','idCalificacion_Video');

         //Obtener las preguntas para un cuestionario.
        $questions = DB::table('cuestionario_Video')
                        ->join('cuestionario_pregunta', 'cuestionario_pregunta.idCuestionario', '=', 'cuestionario_Video.idCuestionario')
                        ->join('preguntas', 'cuestionario_pregunta.idPregunta', '=', 'preguntas.idPregunta')
                        ->select('cuestionario_Video.idVideo','cuestionario_pregunta.idCuestionario', 'preguntas.idPregunta', 'preguntas.pregunta','preguntas.RespuestaA','preguntas.RespuestaB','preguntas.RespuestaC', 'preguntas.Resp_Correcta')
                        ->where('cuestionario_Video.idVideo',$idVideo)
                        ->paginate(1);
   
        return view('video.playVideo', ['detalleVideo' => $videoFind, 
                                        'linkVideo' => $urlPublic.$urlVideo,
                                        'linkVideoOgg' => $urlPublic.$urlVideo_Ogg,
                                        'evaluarVideo' => $calificarVideo,
                                        'question' => $questions
                                        ] ); 
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($idVideo)
    {

        //Buscar el registro del video en la base de datos y en el disco y se elimina
       $RecuperarUrl = DB::table('videos')
                                ->select('url')
                                ->where('idVideo',$idVideo)
                                ->first();

        $values = $RecuperarUrl->{'url'}; 
        
        $nombreFile =  explode('/', $values );

        //\Storage::delete( $nombreFile[8] );
        
        $findVideo = videos::find($idVideo);

        $finVideo->delete();

        //Regresar al index
        $authRol= Auth::user()->role;
        Session::flash('success', 'Eliminado Correctamente '.$nombreFile);
        return redirect()->to($authRol.'/video');
    }

    public function download(Request $request,$idVideo)
    {
        //Recuperar la URL del archivo
        $RecuperarUrl = DB::table('videos')
                                ->select('url')
                                ->where('idVideo',$idVideo)
                                ->first();
        
        $values = $RecuperarUrl->{'url'}; 
        
        $nameExt = explode('/', $values );
        //dd($nameExt);
        $fileRecover = public_path()."/videos/".$nameExt[7];
        return response()->download( $fileRecover );
    }

    public function busquedaVideos(Request $request) {
        
        $titulo = $request->titulo;
        $clasificacion = $request->idClasificacion;
        $tot_Registros = $request->totRegistros;

        //dd($request);
        if($titulo != ""){

            $query = DB::table('videos')
                ->join('clasificaciones', 'videos.clasificacion', '=', 'clasificaciones.idClasificacion')
                ->select('videos.idVideo','videos.titulo','videos.descripcion','videos.duracion', 'clasificaciones.Nombre')
                ->where('videos.titulo', "like" , '%'.$titulo.'%')
                ->orWhere('clasificaciones.idClasificacion', $clasificacion)
                ->orderBy('videos.idVideo', 'ASC')
                ->paginate($tot_Registros);
        }else
        {
            $query = DB::table('videos')
                ->join('clasificaciones', 'videos.clasificacion', '=', 'clasificaciones.idClasificacion')
                ->select('videos.idVideo','videos.titulo','videos.descripcion','videos.duracion', 'clasificaciones.Nombre')
                ->orderBy('videos.idVideo', 'ASC')
                ->paginate($tot_Registros);
        }
        
        $clasificaciones = clasificacionVideo::pluck('Nombre','idClasificacion');
        return view ('video.viewIndex', [
          'videos' => $query,
          'clasificaciones' =>  $clasificaciones
        ]);
    }

   
}
