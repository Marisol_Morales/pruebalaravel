<?php

namespace Biobiblioteca\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Biobiblioteca\preguntaRespuesta;

class preguntaRespuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);        
        //return \Response::json($request->respAlumno);

        $idUsuario = \Auth::id();
        $this->validate($request,[
                    'idPregunta' => 'required',
                    'respAlumno' => ['required', 'max:1'],
        ]);

        //DB::raw('count(pregunta_respuesta.idPregunta) as total')
        $respuestaAlumno = \DB::table('pregunta_respuesta')
                        ->select(\DB::raw('count(pregunta_respuesta.idPregunta) as total'))
                        ->where('pregunta_respuesta.idUsuario', $idUsuario)
                        ->where('pregunta_respuesta.idCuestionario',$request->idCuestionario)
                        ->where('pregunta_respuesta.idPregunta', $request->idPregunta)
                        ->get();
               
        $TotalrespuestasContestadas = (int) $respuestaAlumno[0]->total;
                
        $devuelveRespuestasAlumno = \DB::table('pregunta_respuesta')
                        ->select('pregunta_respuesta.respuesta')
                        ->where('pregunta_respuesta.idUsuario', $idUsuario)
                        ->where('pregunta_respuesta.idCuestionario',$request->idCuestionario)
                        ->get();

    
        if($request->ajax() ){

     
            if( $TotalrespuestasContestadas > 0 ){

               return \Response::json($devuelveRespuestasAlumno->respuesta);
                
            }else{
              $Respuesta = new preguntaRespuesta([
                       'idPregunta' => Input::get('idPregunta'),
                       'respuesta' => Input::get('respAlumno'),
                       'respuestaCorrecta' => Input::get('respuestaCorrecta'),
                       'idCuestionario' => $request->idCuestionario,
                        'idUsuario' => $idUsuario
               ]);
               $Respuesta->save();

               $devuelveRespuestasAlumno = \DB::table('pregunta_respuesta')
                        ->select('pregunta_respuesta.respuesta')
                        ->where('pregunta_respuesta.idUsuario', $idUsuario)
                        ->where('pregunta_respuesta.idCuestionario',$request->idCuestionario)
                        ->get();
                $response = array('status' => 'success',
                                'msg' => 'Respuesta Guardada correctamente',
                                'respuestaAlumno' => $request->respAlumno//$devuelveRespuestasAlumno
                                );
                return \Response::json($response);
            }


        }   
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
