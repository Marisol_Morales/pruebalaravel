<?php

namespace Biobiblioteca\Http\Controllers;

use Illuminate\Http\Request;
use Biobiblioteca\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Biobiblioteca\User;

use Biobiblioteca\video;
use Biobiblioteca\cuestionario;
use Biobiblioteca\clasificacionVideo;
use Biobiblioteca\cuestionarioVideo;


class cuestionarioController extends Controller
{
    protected $authRol;

    public function index(Request $request)
    {
            $cuestionarioAll = cuestionario::Titulo($request->get('titulo'))
                                            ->status($request->get('idStatus'))
                                            ->paginate(10);
            if($request->ajax() ){
               return view('cuestionario.loadCuestionarios', ['cuestionarios' => $cuestionarioAll ])->render(); 
            }

            return view('cuestionario.viewIndex', ['cuestionarios' => $cuestionarioAll ]);  
              
    }

    public function create()
    {
		$clasificaciones = clasificacionVideo::pluck('Nombre','idClasificacion');
        $videos = video::pluck('titulo','idVideo');
        
        return view('cuestionario.createIndex', [
		              'clasificaciones' => $clasificaciones,
                  'videolist' => $videos
		]);
        
    }

    public function store(Request $request)
    {

        if ($request['activo'] ===null) {
            // El usuario No marcó el checkbox 
            $valActivo = 0;
         } else {
           // El usuario Si marcó el chechbox
             $valActivo = 1;
         }


        $this->validate($request,[
                'titulo' => ['required', 'max:150'],
                'clasificaciones' => 'required',
                'videos' => 'required'
            ]);

    $findCuestionarioExistente = \DB::table('cuestionario_Video as cv')
                                    ->whereIn('cv.idVideo', [$request->videos] )
                                    ->value('idCuestionario');
      
    if($findCuestionarioExistente === null){
         $descripcionCampo = 'null';
        //Recuperar y Almacenar los valores en la base de datos
        $tableCuestionario = new cuestionario([
                    'Titulo' => Input::get('titulo'),
                    'idClasificacion' => Input::get('clasificaciones'),
                    'Descripcion' => $descripcionCampo,
                    'activo' => $valActivo
            ]);
        $tableCuestionario->save();
		
        //Recuperar el ultimo registro guardado y lo guardamos en la tabla relacion
        $RecuperarID = DB::table('cuestionarios')
                                         ->select('idCuestionario')
                                         ->orderBy('idCuestionario', 'DESC')
                                         ->first();

        $IDCuestionario = $RecuperarID->{'idCuestionario'};
     
        //Almacena los datos en la tabla relacion
        $tableRelacion = new cuestionarioVideo([
                     'idCuestionario' => $IDCuestionario,
                     'idVideo' =>  Input::get('videos')
            ]);
        $tableRelacion->save();

        //Recupera la informacion y la envia a la vista 
        $Rol = Auth::user()->role;
        return redirect()->to($Rol.'/cuestionario/');
    }else{

            return redirect()->back()->with('success', 'El cuestionario ya tiene un video asignado');

        }

    }

    public function show($idCuestionario)
    {
        //Busca al Cuestionario seleccionado
         $Find = cuestionario::find($idCuestionario);

         $clasificaciones = clasificacionVideo::pluck('Nombre','idClasificacion');
         $videos = video::pluck('titulo','idVideo');

        //Redirecciona al formulario de modificacion
        return view('cuestionario.editIndex', ['test' => $Find, 
                                              'clasificaciones' => $clasificaciones, 
                                              'videolist' => $videos
            ]);
       
    }

    public function edit()
    {
        
    }

    public function update(Request $request, $idCuestionario)
    {
        //Buscamos al registro que se va a eliminar
         $Find = cuestionario::find($idCuestionario);

         if ($request['activo'] ===null) {
            // El usuario No marcó el checkbox 
            $valActivo = 0;
         } else {
           // El usuario Si marcó el chechbox
             $valActivo = 1;
         }
       
        $this->validate($request,[
                'titulo'=> ['required', 'max:150'],
                'clasificaciones' => 'required'
        ]);

        $descripcionCampo = 'null';
        //Se recupera la informacion de la vista y se guarda
        $Find->Titulo = Input::get('titulo');
        $Find->idClasificacion = Input::get('clasificaciones');
        $Find->Descripcion = $descripcionCampo;
        $Find->activo = $valActivo;
        $Find->save();
        
        $Rol = Auth::user()->role;
        //Session::flash('success', 'Cuestionario Modificado ');
        return redirect()->to( $Rol.'/cuestionario' );

    }

    public function destroy($idCuestionario)
    {
        //Busca el registro y lo elimina
        $preguntaDelete = cuestionario::findOrFail($idCuestionario);

        $preguntaDelete-> delete();

        $FindVideo = cuestionarioVideo::find(Input::get('videos'));

        $FindVideo-> delete();

        /*
        return view('cuestionario.viewIndex', [
          'cuestionarios' =>  Cuestionario::all()
        ]);
        */
        Session::flash('success', 'Cuestionario Eliminado ');
        return redirect()->to('cuestionario/index');

    }
}
