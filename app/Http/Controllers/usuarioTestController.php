<?php

namespace Biobiblioteca\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Biobiblioteca\usuarioTest;

class usuarioTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $queryUsuarios = \DB::table('usuarioTest as UT')
            ->join('cuestionarios as C', 'C.idCuestionario', '=', 'UT.idCuestionario')
            ->join('users as U', 'U.id', '=', 'UT.idUsuario')
            ->select(\DB::raw('C.Titulo, U.name, MAX(UT.idCalificacionObtenida) calificacion'))
            ->where('UT.idCalificacionObtenida','>=','90')
            ->groupBy('UT.idCuestionario','C.Titulo', 'UT.idUsuario')
            ->paginate(8);

        if( $request->ajax() ){
            return view('reportes.loadUsers', ['query' => $queryUsuarios])->render();    
        }
        return view('reportes.reporteUsuariosEvaluados', ['query' => $queryUsuarios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       
        //Almacenar el cuestionario respondido
        $numCuestionario = $request->numCuestionario ;//Input::get('numCuestionario');
        /*
        SELECT COUNT(idPregunta)as total FROM `pregunta_respuesta` 
        WHERE idCuestionario = 1 and idUsuario = 2 and respuesta = respuestaCorrecta

        SELECT COUNT(pr.idPregunta)as total 
        FROM pregunta_respuesta pr
        inner join preguntas p on p.idPregunta = pr.idPregunta and p.Resp_Correcta = pr.respuesta
        WHERE idCuestionario = 1 and idUsuario = 2 

        */
        $idUser = $request->userLoggin; //\Auth::id();

        $CuestionarioExist = \DB::table('usuarioTest AS ut')
                            ->where('ut.idUsuario', '=', $idUser)
                            ->where('ut.idCuestionario', $numCuestionario)
                            ->count('ut.idCuestionario');
                            
        $Exist = (int)$CuestionarioExist;
        if( $Exist === 0 ){

           if($request->ajax() ){ 

            $questionsCorrectas = \DB::table('pregunta_respuesta as pr')
                                ->join('preguntas as p', function($join){
                                        $join->on('p.idPregunta', '=', 'pr.idPregunta');
                                        $join->on('p.Resp_Correcta', '=', 'pr.respuesta');
                                })
                                ->select(DB::raw('count(pr.idPregunta) as total'))
                                ->where('pr.idCuestionario', $numCuestionario)
                                ->where('pr.idUsuario', DB::raw($idUser))
                                ->first();
            
           /*
                SELECT count(idPregunta) as total 
                FROM `pregunta_respuesta` 
                WHERE  idCuestionario= 1 and idUsuario = 2

                select p.Pregunta, p.Resp_Correcta,p.solucion  from preguntas p
                INNER join cuestionario_pregunta c on c.idPregunta = p.idPregunta
                where c.idCuestionario = 6
            */
            //dd($request->numCuestionario);
            $devuelveRespuestasAlumno = \DB::table('preguntas as p')
                        ->join('cuestionario_pregunta as c', 'c.idPregunta', '=', 'p.idPregunta')
                        ->select('p.Pregunta','p.Resp_Correcta','p.solucion')
                        ->where('c.idCuestionario',$numCuestionario)
                        ->get();
                if( $questionsCorrectas->total > 0)
                {

                    $tot_Preguntas = \DB::table('cuestionario_pregunta as cp')
                                   ->select(DB::raw('count(cp.idPregunta) as totalPreguntas'))
                                   ->where('cp.idCuestionario', $numCuestionario)
                                   ->first();

                    $calificacion = round(($questionsCorrectas->total/ $tot_Preguntas->totalPreguntas) * 100);

                    $Respuesta = new usuarioTest([
                            'idUsuario' => $idUser,
                            'idCuestionario' => $numCuestionario,
                            'idVideo' => $request->numVideo,
                            'idCalificacionObtenida' => $calificacion
                      ]);
                    $Respuesta->save();
                    
                    $response = array('status' => 'success',
                                        'msg' => 'Evaluacion Finalizada',
                                        'Calificacion' => $calificacion,
                                        'totalRespuestasCorrectas' => $questionsCorrectas->total,
                                        'correctas' => json_encode($devuelveRespuestasAlumno)
                                    );
                    return \Response::json($response);
                }else{

                    $Respuesta = new usuarioTest([
                            'idUsuario' => $idUser,
                            'idCuestionario' => $numCuestionario,
                            'idVideo' => $request->numVideo,
                            'idCalificacionObtenida' => "50"
                    ]);

                    $Respuesta->save();
                     $response = array('status' => 'success',
                                        'msg' => 'Evaluacion Finalizada',
                                        'Calificacion' => '50' ,
                                        'totalRespuestasCorrectas' => $questionsCorrectas->total,
                                        'correctas' => json_encode($devuelveRespuestasAlumno)
                                    );
                    return \Response::json($response);
                }
            }

        }else{
             
             return redirect()->back()->with('alert-Errors', 'Ya ha concluido su evaluacion');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
