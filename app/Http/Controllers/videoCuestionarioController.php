<?php

namespace Biobiblioteca\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Biobiblioteca\video;
use Biobiblioteca\cuestionario;
use Biobiblioteca\clasificacionVideo;
use Biobiblioteca\cuestionarioVideo;

class videoCuestionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clasificaciones = clasificacionVideo::pluck('Nombre','idClasificacion');
        $videos = video::orderBy('idVideo', 'ASC')->paginate(10);

        return  view('cuestionario.addCuestionarioVideo', ['videos' => $videos, 'clasificaciones' => $clasificaciones]);
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
       
        //Almacenar el video relacionado con el cuestionario
        $videoEdit = video::find($request->idVideo);
        $videoEdit->titulo= $request->nombre;
        if($videoEdit === null){
            //No tiene un cuestionario asignado por lo tanto se crea
            return redirect()->back()->with('success', 'El video no se encontro');

        }else{

            $videoEdit->save();

            return redirect()->back()->with('success', 'El video ha sido actualizado');
        }

        
    }

    public function show(Request $request,$idVideo)
    {
        $path = $request->url();
        //$array_Path = explode('/', "http://ibhu.noip.me:6080/index.php/administrador/usuarioTest");
        $array_Path = explode('/', $path);
        $URL_Path = $array_Path[0].'//'.$array_Path[2];

        $urlPublic = $URL_Path; //'http://pruebalaravel';

        $videoFind = video::find($idVideo);

        $videoUrl =  DB::table('videos')
                                ->select('url')
                                ->where('idVideo',$idVideo)
                                ->first();
       $values = $videoUrl->{'url'};
      
        $nameExt = explode('/', $values);

        $UrlFile = '/'.$nameExt[6].'/'.$nameExt[7];
        
        $urlVideo_Ogg = str_replace('mp4', 'ogg', $UrlFile);

       
        $cuestionario = cuestionario::pluck('titulo','idCuestionario');
        return  view('video.addCuestionario', ['listaCuestionario' => $cuestionario,
                                               'videoDetail' => $videoFind,
                                               'linkVideo' => $urlPublic.$UrlFile,
                                               'linkVideoOgg' => $urlPublic.$urlVideo_Ogg,
                                               'idVideoVal' => $idVideo
                                            
        ]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
