<?php

namespace Biobiblioteca\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Biobiblioteca\User;


class userController extends Controller
{
     public function index(Request $request )
    {
       //Se hace un llamado al modelo
       $userlist = User::orderBy('id','ASC')
                                ->paginate(8);
        if( $request->ajax() ){
            return view('usuarios.loadUsuarios',['listUser' => $userlist])->render();    
        }
       //Se devuelven los datos en una variable mostrada en la vista
        return view('usuarios.userIndex',['listUser' => $userlist]);
    }

     public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
       //Se busca el valor de la clasificacion
        $find = User::find($id);

        return view('usuarios.edit' , ['userFind' => $find] );
    }

    public function edit($id)
    {
     
    }

    public function update(Request $request, $id)
    {
        //Busca la clasificacion a modificar
        $find = User::find($id);
         
        $this->validate($request,[
                'nombre'=> ['required', 'max:100'],
                ]);

        //Guarda la informacion modificada
        $find->name = $request->input('nombre');
        $find->role  = $request->input('tipoRol');
        $find->save();

        //Regresamos a la vista del listado de clasificaciones
        $user = \Auth::user()->role;
        return redirect()->to($user.'/user');
    }

    public function destroy($id)
    {
        //Buscar y eliminar el registro seleccionado
        User::destroy($id);
        $user = \Auth::user()->role;
        return redirect()->to($user.'/user');
    }
}
