<?php

namespace Biobiblioteca\Http\Controllers;

use Illuminate\Http\Request;

use Biobiblioteca\Http\Requests;
use Biobiblioteca\Pregunta;
use Biobiblioteca\video;
use Biobiblioteca\cuestionariopregunta;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class preguntaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) 
    {
        //dd($request);
        $listaPreguntas = Pregunta::cuestionario($request->get('titulo'))->paginate(10);
       
        if($request->ajax() ){
            return view('pregunta.loadPregunta',['preguntas' => $listaPreguntas])->render();    
        }

        return view('pregunta.viewIndex', ['preguntas' => $listaPreguntas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $input = Input::all();
      $idCuestionario = $input['idCuestionario'];
      //return view('pregunta.createIndex', ['new' => true, 'preguntas' => new Pregunta, 'CuestionarioID' => $idCuestionario]);
      return view('pregunta.createIndex', ['CuestionarioID' => $idCuestionario]);
          
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validar Datos
        $this->validate($request,[
			'pregunta'=> 'required',
			'respuestaA' => 'required',
			'respuestaB' => 'required',
			'respuestaC' => 'required',
			'respuestaCorrecta' => ['required', 'max:1'],
            'solucion' => 'required'
		]);

        //Recuperar los valores del formulario
        //Modelo que recopila la informacion que sera enviada la base de datos      
        $entry = new Pregunta([
	      'Pregunta' => Input::get('pregunta'),
	      'RespuestaA' => Input::get('respuestaA'),
	      'RespuestaB' => Input::get('respuestaB'),
	      'RespuestaC' => Input::get('respuestaC'),
	      'Resp_Correcta' => Input::get('respuestaCorrecta'),
          'solucion' => Input::get('solucion')
        ]);
        $entry->save();

        //Recuperar el ultimo registro guardado y lo guardamos en la tabla relacion
        $RecuperarID = DB::table('preguntas')
                                ->select('idPregunta')
                                ->orderBy('idPregunta', 'DESC')
                                ->first();

        $idPregunta = $RecuperarID ->{'idPregunta'};

        $entryRelacion = new cuestionariopregunta([
                        'idCuestionario' => Input::get('Cuestionario_ID'),
                        'idPregunta' => $idPregunta
        ]);
        $entryRelacion-> save();

        //Envia al listado de preguntas para ver todas las preguntas
        Session::flash('flash_message', 'Pregunta Agregada!!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idPregunta)
    {
        $preguntaFind = Pregunta::find($idPregunta);
        
        return view('pregunta.editIndex', ['question' => $preguntaFind] ); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idPregunta)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $idPregunta , Request $request)
    {
         //Busca y edita la pregunta
         $preguntaFind = Pregunta::find($idPregunta);
         
         $this->validate($request,[
                'pregunta'=> 'required',
                'respuestaA' => 'required',
                'respuestaB' => 'required',
                'respuestaC' => 'required',
                'respuestaCorrecta' => ['required', 'max:1']
                ]);

         //Guardar la informacion 
         $preguntaFind->Pregunta = Input::get('pregunta');
         $preguntaFind->RespuestaA = Input::get('respuestaA');
         $preguntaFind->RespuestaB = Input::get('respuestaB');
         $preguntaFind->RespuestaC = Input::get('respuestaC');
         $preguntaFind->Resp_Correcta = Input::get('respuestaCorrecta');
         $preguntaFind->solucion = Input::get('solucion');
         $preguntaFind->save();

         //Retorna al listado de vista de las preguntas
         //return view('pregunta.viewIndex', ['preguntas' => Pregunta::all() ]);
         $Rol = \Auth::user()->role;
         return redirect()->to($Rol.'/pregunta');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idPregunta)
    {
        //Busca el registro y lo elimina
        Pregunta::destroy($idPregunta);

        return view('pregunta.viewIndex', [
          'preguntas' => Pregunta::all()
        ]);
    }
}
