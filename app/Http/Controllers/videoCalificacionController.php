<?php

namespace Biobiblioteca\Http\Controllers;

use Illuminate\Http\Request;
use Biobiblioteca\videoCalificacion;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Khill\Lavacharts\Lavacharts;

class videoCalificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $lava = new Lavacharts; // See note below for Laravel
        /*
        $finances = $lava->DataTable();
        
        $finances->addDateColumn('Year')
                 ->addNumberColumn('Sales')
                 ->addNumberColumn('Expenses')
                 ->setDateTimeFormat('Y')
                 ->addRow(['2004', 1000, 400])
                 ->addRow(['2005', 1170, 460])
                 ->addRow(['2006', 660, 1120])
                 ->addRow(['2007', 1030, 54]);

        $lava->ColumnChart('Finances', $finances, [
            'title' => 'VIDEOS MAS VISITADOS',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ]
        ]);
        */
        $queryVideos = \DB::table('videoCalificacion as vc')
                            ->join('videos as v', 'v.idVideo', '=', 'vc.idVideo')
                            ->select(\DB::raw('v.titulo , count(vc.idVideo) totalVideo'))
                            ->groupBy('vc.idVideo')
                            ->orderBy('totalVideo','DESC')
                            ->paginate(8);

        if( $request->ajax() ){
            return view('reportes.loadVideos',['query' => $queryVideos])->render();    
        }

        return view('reportes.reporteVideosVisitados', ['query' => $queryVideos]);
    }

    public function create(Request $request)
    {
        
    }

    public function store(Request $request)
    {
       //Asignar una calificacion al video visto
        $califica_Video = new videoCalificacion([
                'idVideo' => Input::get('videoID'),
                'idCalificacion_Video' => Input::get('cal_Video')
            ]);
        $califica_Video->save();
         Session::flash('success', 'Video calificado');
         return redirect()->back();
    }

    public function show($id)
    {
        //return view('reporte.viewIndex', ['preguntas' => $listaPreguntas]);
        
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request)
    {
        //
       
    }

    public function destroy($id)
    {
        //
    }
}
