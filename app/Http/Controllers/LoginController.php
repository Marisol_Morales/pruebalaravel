<?php

namespace Biobiblioteca\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use Biobiblioteca\User;
use Auth;

class LoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest', ['except' => ['logout', 'getLogout']]);
    }

    public function login()
    {
    	return view('auth.login');
    }

    public function posLogin(Request $request){
    	
    	  $tvar = $request['username'];
        $pw = $request['password'];
        $UserRol = $request['roleUser'];

        if (Auth::attempt(['email' => $tvar, 'password' => $pw, 'role' => $UserRol ]))
        {
            $userName = Auth::user();

            if($userName->role != $UserRol)
            {
                //Session::flash();
                return redirect()->back()->with('success', 'Rol de usuario no corresponde al registrado');   
            }
            else{

                return redirect('/'.$userName->role)->with($userName->role);
            }
            
        }
        else
        {
           
            return redirect()->to('/login')->with('success', 'Usuario y/o contraseña no reconocidos');   
        }
    
    }

    public function getLogout()
    {
         \Auth::logout();
         return view('welcome');
    }

    public function getRegister()
    {
    
       return view('auth.register');
       
    }

   public function create(Request $data)
   {
    
      try{

            $empty = new User([
              'name' => $data['name'],
              'email' => $data['email'],
              'password' => bcrypt($data['password']),
              'role' => $data['role']
            ]);
            $empty->save();
            //return view('auth.login');
            return redirect()->to('login');

        }catch(\Illuminate\Database\QueryException $e){
            return redirect()->back()->with('message', 'El usuario ya esta registrado');
         }
    
      //~ dd($data);
     
    }

}
