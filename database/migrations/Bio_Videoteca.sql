-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 15-12-2016 a las 19:35:12
-- Versión del servidor: 5.7.12-0ubuntu1
-- Versión de PHP: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Bio_Videoteca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calificacionVideo`
--

CREATE TABLE `calificacionVideo` (
  `idCalificacion_Video` int(11) NOT NULL,
  `calificacion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clasificaciones`
--

CREATE TABLE `clasificaciones` (
  `idClasificacion` int(4) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Descripcion` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clasificaciones`
--

INSERT INTO `clasificaciones` (`idClasificacion`, `Nombre`, `Descripcion`) VALUES
(1, 'Biologia', 'Temas generales de Biologia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuestionarios`
--

CREATE TABLE `cuestionarios` (
  `idCuestionario` int(6) NOT NULL,
  `Titulo` varchar(300) NOT NULL,
  `idClasificacion` int(6) NOT NULL,
  `Descripcion` varchar(400) DEFAULT NULL,
  `activo` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuestionarios`
--

INSERT INTO `cuestionarios` (`idCuestionario`, `Titulo`, `idClasificacion`, `Descripcion`, `activo`) VALUES
(1, 'Cuestionario de las Vias Urinarias', 1, 'Evaluacion para alumnos de pregrado', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuestionario_pregunta`
--

CREATE TABLE `cuestionario_pregunta` (
  `idCuestionario` int(6) NOT NULL,
  `idPregunta` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuestionario_pregunta`
--

INSERT INTO `cuestionario_pregunta` (`idCuestionario`, `idPregunta`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE `preguntas` (
  `idPregunta` int(6) NOT NULL,
  `Pregunta` varchar(400) NOT NULL,
  `Respuesta` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`idPregunta`, `Pregunta`, `Respuesta`) VALUES
(1, 'Que es una infeccion urinaria?', 'A'),
(2, '¿Cual es el nivel de Hemoglobina en una persona adulta?', 'C'),
(3, 'Menciona un sintomas principal del Virus SIKA?', 'B');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pregunta_respuesta`
--

CREATE TABLE `pregunta_respuesta` (
  `idPregunta` int(6) NOT NULL,
  `respuesta` varchar(5) NOT NULL,
  `respuestaCorrecta` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(9) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `fecha_Ingreso` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarioTest`
--

CREATE TABLE `usuarioTest` (
  `idUsuario` int(9) DEFAULT NULL,
  `idCuestionario` int(6) DEFAULT NULL,
  `idVideo` int(11) DEFAULT NULL,
  `idCalificacionObtenida` decimal(4,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videoCalificacion`
--

CREATE TABLE `videoCalificacion` (
  `idVideo` int(11) DEFAULT NULL,
  `idCalificacion_Video` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `idVideo` int(11) NOT NULL,
  `url` varchar(500) DEFAULT NULL,
  `titulo` varchar(300) DEFAULT NULL,
  `ancho` int(6) DEFAULT NULL,
  `alto` int(6) DEFAULT NULL,
  `framerate` int(8) DEFAULT NULL,
  `duracion` time DEFAULT NULL,
  `clasificacion` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `calificacionVideo`
--
ALTER TABLE `calificacionVideo`
  ADD PRIMARY KEY (`idCalificacion_Video`);

--
-- Indices de la tabla `clasificaciones`
--
ALTER TABLE `clasificaciones`
  ADD PRIMARY KEY (`idClasificacion`);

--
-- Indices de la tabla `cuestionarios`
--
ALTER TABLE `cuestionarios`
  ADD PRIMARY KEY (`idCuestionario`);

--
-- Indices de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD PRIMARY KEY (`idPregunta`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`idVideo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clasificaciones`
--
ALTER TABLE `clasificaciones`
  MODIFY `idClasificacion` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `cuestionarios`
--
ALTER TABLE `cuestionarios`
  MODIFY `idCuestionario` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `idVideo` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
