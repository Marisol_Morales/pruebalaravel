<?php

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

//Route::get('/', 'HomeController@index');

//Rutas del acceso a usuarios
Route::get('login', [
			'uses' => 'LoginController@login',
			'as' => 'login'
	]);
Route::post('login', 'LoginController@posLogin');
Route::get('logout', 'LoginController@getLogout');


//Rutas del registro a usuarios
Route::get('/registro', 'LoginController@getRegister');
Route::post('/registro', 'LoginController@create');

Route::resource('/video/download', 'videoController@download');

//Rutas de usuarios Tipo - Alumnos
Route::group( ['prefix' => 'alumno'] , function(){


	Route::get('/', function () {
   					 return view('UsuarioRol.alumno', ['Tipo_Acceso' => 'alumno']);
	});
	
	Route::resource('video', 'videoController');

	Route::resource('cuestionario', 'cuestionarioController');

	Route::resource('videoCalificacion', 'videoCalificacionController');

	Route::resource('preguntaRespuesta', 'preguntaRespuestaController');
	//Route::post('preguntaRespuesta', 'preguntaRespuestaController@store');

	Route::resource('usuarioTest', 'usuarioTestController');


});



//Rutas de usuario Tipo - Docente
Route::group( ['prefix' => 'docente'] , function(){

	Route::get('/', function () 
	{
		return view('UsuarioRol.docente', ['Tipo_Acceso' => 'docente']);
	});

	Route::resource('video', 'videoController');

	Route::resource('videoCuestionario', 'videoCuestionarioController');

	Route::resource('cuestionario', 'cuestionarioController');

	Route::resource('clasificacionVideo', 'clasificacionVideoController');
	
	Route::resource('pregunta', 'preguntaController');

	Route::resource('usuarioTest', 'usuarioTestController');

	Route::resource('preguntaRespuesta', 'preguntaRespuestaController');

});



//Rutas de usuario Tipo - Administrador
Route::group(['prefix' => 'administrador']  , function(){
//Route::group(['middleware' => 'administrador']  , function(){
	
	Route::get('/', function () 
	{
		return view('UsuarioRol.administrador');
	});

	Route::get('busquedaVideos', 'videoController@busquedaVideos');
	Route::resource('video', 'videoController');

	Route::resource('cuestionario', 'cuestionarioController');

	Route::resource('clasificacionVideo', 'clasificacionVideoController');

	Route::resource('pregunta', 'preguntaController');

	Route::resource('videoCuestionario', 'videoCuestionarioController');

	Route::resource('preguntaRespuesta', 'preguntaRespuestaController');

	Route::resource('usuarioTest', 'usuarioTestController');

	Route::resource('user', 'userController');
	
});
