;

<?php $__env->startSection('title', 'BioVideoteca >> Creacion de Preguntas'); ?>

<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	
<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-gift"></i>Listado de Cuestionarios
    </div>
  </div>

<div class="portlet-body">
    <div class="panel-body">
          <?php echo Form::open(['route'=> 'cuestionario.index', 'method'=> 'GET', 'class'=> 'navbar-form navbar-left pull-right', 'role'=> 'search']); ?>

            <div class="form-group">
              <?php echo Form::text('titulo', null, ['class' => 'form-control', 'placeholder' => 'Buscar por cuestionario' ]); ?>

              <?php echo Form::select('idStatus', array('2'=>'seleccione', '1' => 'Activo', '0' => 'inactivo'),  null, array('class' => 'form-control')); ?>

            </div>
            <button type="submit" class="btn btn-default">Buscar</button>
          <?php echo Form::close(); ?>

    </div>
<div class="table-responsive">

  <?php if($cuestionarios->count()): ?>
    <?php echo $__env->make('cuestionario.loadCuestionarios', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php else: ?>
    <h2>No se encontraron cuestionarios registrados</h2>
  <?php endif; ?>
</div>
  <?php $__env->stopSection(); ?>

<?php $__env->startSection('page_script'); ?> 
  <script>
  $(document).ready(function(){
    
    //Actualiza el logo de la aplicacion de Videoteca
    var currentPage = window.location.href.split('/');
    var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
    var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
    $('#logoVideoteca').attr('src', urlLogo);


    $(document).on('click','.pagination a', function(e){
      e.preventDefault();
      var url = $(this).attr('href');
      getCuestionarios(url);
    });
  });

  function getCuestionarios(url){

    $.ajax({
      url: url
    }).done(function(data){
      $('.table-responsive').html(data);
      //console.log(data);
      
    }).fail(function (){
      alert('No se pudo leer la informacion');
    });

  }
    
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>