<?php $__env->startSection('title', 'BioVideoteca >> Creacion de Preguntas'); ?>

<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="portlet box blue">
  <div class="portlet-title">
  <div class="caption">
  	 Agregar un nuevo Cuestionario
  </div>
  </div>
</div>

<div class="portlet-body form">
     
<div class="form-body">
   <div class="form-horizontal">
<?php echo $__env->make('layout/partials/errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo Form::open(['route' => 'cuestionario.store']); ?>

     <div class="form-group">
	     <label class="col-md-3 control-label">Titulo del cuestionario</label> 
	     <div class="col-md-9" >
			    <?php echo e(Form::text('titulo', null,  ['class' => 'form-control'])); ?>

       </div>
      </div>

     <div class="form-group">
			<label class="col-md-3 control-label">Listado de videos</label>
			 <div class="col-md-9" >
			     <?php echo e(Form::select('videos', $videolist, null, array('class' => 'form-control'))); ?>

        </div>
	    </div>
     
      <div class="form-group">
	      <label class="col-md-3 control-label">Clasificacion del Cuestionario</label>
			  <div class="col-md-9" >
            	<?php echo e(Form::select('clasificaciones', $clasificaciones, null, array('class' => 'form-control'))); ?>

        </div>
		  </div>
       <!-- 
      <div class="form-group">
     
      <label class="col-md-3 control-label">Descripcion del Cuestionario</label>
        <div class="col-md-9">
	        <?php echo e(Form::textArea('descripcion', null,  ['class' => 'form-control'])); ?>

	      </div>
      </div>
      -->
      
      <div class="form-group">
          <label class="col-md-3 control-label">Activo </label> 
        <div class="col-md-9">
          <?php echo e(Form::checkbox('activo', 0, null, ['class' => 'field'])); ?>

        </div>
      </div>
      
      <div class="form-group">
        <div class="col-xs-12 col-md-12 form-group">
	         <button class="btn btn-primary pull-right" type="reset">Limpiar</button>
	         <button class="btn btn-primary pull-right" type="submit">Guardar</button>
        </div>
      </div>
      </div>
</div> 
<?php echo Form::close(); ?>

</div>
<?php $__env->stopSection(); ?>                 

<?php $__env->startSection('page_script'); ?> 
   <script>
    $(document).ready(function(){
        //Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>