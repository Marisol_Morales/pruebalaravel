
<?php $__env->startSection('title', 'BioVideoteca >> Creacion de videos'); ?>

<?php $__env->startSection('breadcrumbs'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

        <!-- Keep all page content within the page-content inset div! -->
<div class="portlet box purple">
  <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-gift"></i>Registro de Videos
    </div>
 </div>

<div class="portlet-body">
 <?php if(Session::has('success')): ?>
  <div class="alert alert-info">
    <h2><?php echo Session::get('success'); ?></h2>
  </div>
 <?php endif; ?>

 <?php echo Form::open(array('route' => 'video.store', 'files'=>true )); ?>

 <?php echo e(csrf_field()); ?>


	<div class="form-body"> 
          <label class="control-label">Titulo del video:</label>
          <div class="input-group" >
            <!--<?php echo e(Form::input('text', 'titulo')); ?> -->
            <input type="text" class= "form-control input-circle" name="txttitulo" id="titulo" value="<?php echo e(old('titulo')); ?>"> 
          </div>
          <div class="form-group">
          <label class="control-label">Clasificaciòn:</label>
          <div class="input-group" >
            <?php echo e(Form::select('clasificaciones', $clasificaciones, null, array('class' => 'form-control'))); ?>

          </div>
    	 </div> 
          <div class="form-group">
		  <label class="control-label">Archivo</label>
		  <div class="form-group">
		       <input type="file" class= "" name="archivo" id="url" value="<?php echo e(old('url')); ?>"> 
		  </div>
		  </div>
        
          <label class="control-label">Descripcion:</label>
          <div class="input-group" >
            <?php echo e(Form::textarea('descripcion')); ?>

          </div>

      <div class="input-group" >
     
		   <input type ="submit" class="btn btn-circle purple" value="Guardar video">
      
       <input type ="reset" class="btn btn-circle purple" value="limpiar">
       
	 	  </div>
       </div>
 <?php echo Form::close(); ?>



  </div>
  </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>