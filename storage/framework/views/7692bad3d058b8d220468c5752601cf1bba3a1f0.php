<?php if(Session::has('success')): ?>
					    <div class="alert alert-success">
					        <?php echo e(Session::get('success')); ?>

					    </div>
					<?php endif; ?>
<table class="table table-striped table-bordered table-hover" id="tblVideo">
					<thead>
					<tr>
						<th>Nº</th>
						<th>Titulo</th>
						<th align="center">Descripcion</th>
						<th align="center">Duracion</th>
						<th align="center">Clasificacion</th>
					    <?php if(Auth::user()->role != 'alumno'): ?>
						<th align="center">Editar</th>
						<?php endif; ?>
						<?php if(Auth::user()->role === 'administrador'): ?>
					    <th>Eliminar</th>
					    <?php endif; ?>
					    <th>Ver</th>
					    <?php if(Auth::user()->role === 'administrador'): ?>
					    <th>Download</th>
					    <?php endif; ?>
					</tr>
				  </thead>
				  	<tbody>
				  	<?php $__currentLoopData = $videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
				  	<tr>
						<td><?php echo e($item->idVideo); ?></td>
					   	<td><?php echo e($item->titulo); ?></td>
					   	<td><?php echo e($item->descripcion); ?></td>
					  	<td><?php echo e($item->duracion); ?></td>
					  	<td><?php echo e($item->Nombre); ?></td>
					  	<?php if(Auth::user()->role != 'alumno'): ?>
					   	<td>
					   		<a href="<?php echo e(route('videoCuestionario.show', $item->idVideo)); ?>" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
					   	</td>
					   	<?php endif; ?>
					   	<?php if(Auth::user()->role === 'administrador'): ?>
					   	<td>
					   		<a href="<?php echo e(route('video.destroy', $item->idVideo)); ?>" class="btn btn-danger"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
					   	</td>
					   	<?php endif; ?>
					   	<td><a href="<?php echo e(route('video.show', $item->idVideo)); ?>" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
					   	<?php if(Auth::user()->role === 'administrador'): ?>
					   	<td><a href="<?php echo e(url('video/download', $item->idVideo)); ?>" class="btn btn-success"><span class="glyphicon glyphicon-save" aria-hidden="true"></span></a></td>
					   	<?php endif; ?>
					</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
					</tbody>
				</table>
				<div class="form-group">
					<table>
						<thead>
							<tr>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php echo e($videos->links()); ?></td>
								<td  align="right"><?php echo e($videos->firstItem()); ?> de <?php echo e($videos->count()); ?> en total <?php echo e($videos->total()); ?></td>
							</tr>
						</tbody>
					</table>
					
</div>