<div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                            <li class="nav-item start ">
                                <img src="../assets/layouts/layout6/img/logo1.png" id="logoVideoteca"> 
                                <a href="/index.php/<?php echo e(Auth::user()->role); ?>" class="nav-link nav-toggle">
                                    <span class="title">Menu principal</span>
                                     <h2 class="uppercase" align="center"><?php echo e(Auth::user()->role); ?> </h2>
                                </a>
                             </li>
                           <li class="heading">
                                <h3 class="uppercase">Operaciones</h3>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-camcorder"></i>
                                    <span class="title">Video</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                     <li class="nav-item  ">
                                        <a href="<?php echo e(url('alumno/video')); ?>" class="nav-link ">
                                            <span class="title">Examinar</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">Cuestionario</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo e(url('alumno/cuestionario/' )); ?>" class="nav-link ">
                                            <span class="title">Examinar</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
</div>
