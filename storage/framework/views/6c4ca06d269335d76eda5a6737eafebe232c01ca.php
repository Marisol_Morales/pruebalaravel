                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                            <li class="nav-item start ">
                            </script>
                            <img src="../assets/layouts/layout6/img/logo1.png" id="logoVideoteca"> 
                            <a href="/index.php/<?php echo e(Auth::user()->role); ?>" class="nav-link nav-toggle">
                                <span class="title">Menù principal</span>
                                <h2 class="uppercase" ><?php echo e(Auth::user()->role); ?> </h2>
                            </a>
                            </li>
                            <li class="heading">
                                <h3 class="uppercase">Operaciones</h3>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-film"></i>
                                    <span class="title">Video</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo e(route('video.create', [])); ?>" class="nav-link ">
                                            <span class="title">Agregar</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo e(route('video.index', [])); ?>" class="nav-link ">
                                            <span class="title">Examinar</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-notebook"></i>
                                    <span class="title">Cuestionario</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo e(route('cuestionario.create', [])); ?>" class="nav-link ">
                                            <span class="title">Agregar</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo e(route('cuestionario.index', [])); ?>" class="nav-link ">
                                            <span class="title">Examinar</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-note"></i>
                                    <span class="title">Preguntas</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo e(route('pregunta.index', [])); ?>" class="nav-link ">
                                            <span class="title">Examinar</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-layers"></i>
                                    <span class="title">Clasificaciones</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo e(route('clasificacionVideo.create', [])); ?>" class="nav-link ">
                                            <span class="title">Agregar</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo e(route('clasificacionVideo.index', [])); ?>" class="nav-link ">
                                            <span class="title">Examinar</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="heading">
                                <h3 class="uppercase">Reportes</h3>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link ">
                                     <i class="icon-docs"></i>
                                    <span class="title">Videos mas visitados</span>
                                     <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo e(route('videoCalificacion.index', [])); ?>" class="nav-link ">
                                            <span class="title">Examinar</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="jnav-item  ">
                                <a href="javascript:;" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">Usuarios destacados</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?php echo e(route('usuarioTest.index', [])); ?>" class="nav-link ">
                                            <span class="title">Examinar</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?php echo e(route('user.index', [])); ?>" class="nav-link ">
                                            <span class="title">Usuarios Registrados</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
