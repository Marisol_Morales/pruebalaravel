        <!-- BEGIN HEADER -->
        <header class="page-header">
            <nav class="navbar" role="navigation">
                <div class="container-fluid">
                    <div class="havbar-header">
                        <!-- BEGIN LOGO -->
                         <?php if(Auth::guest()): ?> 
                        <a id="index" class="navbar-brand" href="/index.php"> <h1><span class="font-yellow-crusta" >VIDEOTECA DE BIOMEDICA y GASTROENTEROLOGIA, UANL</h1>
                        </a>
                        <?php else: ?>
                        <a id="index" class="navbar-brand" href="/index.php/<?php echo e(Auth::user()->role); ?>"> <h1><span class="font-yellow-crusta" >VIDEOTECA DE BIOMEDICA y GASTROENTEROLOGIA, UANL</h1>
                        </a>
                        <?php endif; ?>
                        <!-- END LOGO -->
                        <!-- BEGIN TOPBAR ACTIONS -->
                        <div class="topbar-actions">

                            <!-- BEGIN HEADER SEARCH BOX -->
                         <?php if(Auth::guest()): ?>   
                            <div class="btn-group-notification btn-group" id="header_notification_bar">
                                <button type="button" class="btn md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                  </button>
                                <ul class="dropdown-menu-v2">
                                    <li class="external">
                                        <h3>
                                        <span class="bold">12 pending</span> notifications</h3>
                                    </li>
                        
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px; padding: 0;" data-handle-color="#637283">
                                            <li>
                                                <a href="/index.php/registro">
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success md-skip">
                                                            <i class="fa fa-plus"></i>
                                                        </span> Registrarse </span>
                                                </a>
                                            </li>
                                            <li>
                                                 <a href="/index.php/login">
                                                <i class="icon-key"></i> Iniciar sesion </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!-- END GROUP NOTIFICATION -->

                            <!-- BEGIN USER AUTHENTICATION -->
                        <?php else: ?> 
                            <label class="font-purple-seance"><h3><?php echo e(Auth::user()->name); ?></h3></label>
                            <div class="btn-group-img btn-group">
                                <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                  <img src="../assets/layouts/layout6/img/avatar1.jpg" alt=""> 
                                </button>
                                <ul class="dropdown-menu-v2" role="menu">
                                    <li class="divider"> </li>
                                 
                                   <li>
                                        <a href="/index.php/logout">
                                            <i class="icon-lock"></i> Cerrar Sesion </a>
                                    </li>
                               
                                </ul>
                            </div>
                        <?php endif; ?>
                            <!-- END USER PROFILE -->
                        </div>
                        <!-- END TOPBAR ACTIONS -->
                    </div>
                </div>
                <!--/container-->
            </nav>
        </header>
        <!-- END HEADER -->
