<!DOCTYPE html>
<?php echo $__env->make('layout.partials.metas', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
        <?php echo $__env->make('layout.partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- Specific Style Sheets -->
        <?php echo $__env->yieldContent('customCSS'); ?>
    <!-- END HEAD -->

    <body class="">
        <?php echo $__env->make('layout.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <!-- BEGIN CONTAINER -->
        <div class="container-fluid">
            <div class="page-content page-content-popup">
                <div class="page-content-fixed-header">
                    <!-- BEGIN BREADCRUMBS -->
                    <?php echo $__env->yieldContent('breadcrumbs'); ?>
                    <!-- END BREADCRUMBS -->
                    <div class="content-header-menu">
                      
                    </div>
                </div>
                <?php echo $__env->make('layout.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="page-fixed-main-content">
<!-- =================== BEGIN PAGE BASE CONTENT =================== -->
                    <?php echo $__env->yieldContent('content'); ?>
<!-- =================== END PAGE BASE CONTENT =================== -->
                </div>

               
            </div>
        </div>
        <!-- END CONTAINER -->
     
        <?php echo $__env->make('layout.partials.foot', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('layout.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('page_script'); ?>
    </body>

</html>
