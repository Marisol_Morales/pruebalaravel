<?php $__env->startSection('title', 'BioVideoteca >> Cargar videos'); ?>

<?php $__env->startSection('breadcrumbs'); ?>

@endsections

<?php $__env->startSection('content'); ?>

<div class="portlet box blue">
 <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-gift"></i>
      Agregar un nuevo Video
    </div>
  </div>

<div class="portlet-body form">
  <div class="form-body">
    <div class="form-horizontal">
    
<?php if(Session::has('success')): ?>
    <div class="alert alert-success">
        <?php echo e(Session::get('success')); ?>

    </div>
<?php endif; ?>

<?php echo e(Form::open(array('route' => 'video.store', 'files'=>true ))); ?>

         <?php echo Form::hidden('_token', csrf_token()); ?>

        <div class="form-group">
           <label class="col-md-3 control-label">Titulo</label>
           <div class="col-md-9">
            <?php echo e(Form::text('titulo',null,array('class'=>'form-control'))); ?>

           </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Descripcion del video</label>
            <div class="col-md-9">
            <?php echo e(Form::text('descripcion',null,array('class'=>'form-control'))); ?>

            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Video seleccione</label>
            <div class="col-md-9">
            <?php echo e(Form::file('archivo','',array('id'=>'','class'=>''))); ?>

            </div>
        </div>

        <div class="form-group">
          <label class="col-md-3 control-label">Clasificacion del Video</label>
          <div class="col-md-9" >
                <?php echo e(Form::select('Listaclasificacion', $clasificaciones, null, array('class' => 'form-control'))); ?>

          </div>
        </div>

        <div class="form-group">
          <div class="col-md-9" >
            <?php echo e(Form::submit('Agregar',array('class'=>'btn btn-primary'))); ?>

            <?php echo e(Form::reset('Limpiar',array('class'=>'btn btn-success'))); ?>

          </div>  
        </div>
<?php echo e(Form::close()); ?>

        </div>
      </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_script'); ?> 
   <script>
    $(document).ready(function(){
        //Actualiza el logo de la aplicacion de Videoteca
        var currentPage = window.location.href.split('/');
        var path = currentPage[0].split('.')[0] + '//' + currentPage[2].split('.')[0];
        var urlLogo = path + '/assets/layouts/layout6/img/logo1.png';
        $('#logoVideoteca').attr('src', urlLogo);

    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>