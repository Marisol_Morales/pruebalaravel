<?php if($errors->count()): ?>
		<div class="alert alert-danger">
		  <p><strong>¡Atención!</strong> Se han presentado errores en el formulario</p>
		  <ul>
		  <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
		    <li><?php echo e($error); ?></li>
		  <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		  </ul>
		</div>
<?php endif; ?>

<?php if(Session::has('success')): ?>
    <div class="alert alert-success">
      <?php echo e(Session::get('success')); ?>

    </div>
<?php endif; ?>