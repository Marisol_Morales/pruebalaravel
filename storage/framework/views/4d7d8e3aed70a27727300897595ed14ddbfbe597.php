
<!DOCTYPE html>
<?php echo $__env->make('layout.partials.metas', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<html lang="en">
     <!-- BEGIN HEAD -->
        <?php echo $__env->make('layout.partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <title><h1><?php echo e(config('app.name', 'Videoteca')); ?></h1></title>

        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">

        <!-- Specific Style Sheets -->
        <?php echo $__env->yieldContent('customCSS'); ?>
    <!-- END HEAD -->
    <style>
    .login{
     background-color: #171a1e;
    }
    </style>
    <body class="login">

        <div id="app">
           <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                        <?php echo e(config('app.name', 'Videoteca')); ?>

                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    
                </div>
            </div>
        </nav>
       
    </div>

        <!-- BEGIN CONTAINER -->
        <div class="container-fluid">
                <div class="page-content-fixed-header">
                    <!-- BEGIN BREADCRUMBS -->
                    <?php echo $__env->yieldContent('breadcrumbs'); ?>
                    <!-- END BREADCRUMBS -->
                    <div class="content-header-menu">
                      
                    </div>
                </div>
             
                <div class="page-fixed-main-content">
<!-- =================== BEGIN PAGE BASE CONTENT =================== -->
                    <?php echo $__env->yieldContent('content'); ?>
<!-- =================== END PAGE BASE CONTENT =================== -->
                </div>

                <!-- BEGIN FOOTER - ->
              
                <!-- END FOOTER -->
            
        </div>
        <!-- END CONTAINER -->
         <?php echo $__env->make('layout.partials.foot', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldContent('page_script'); ?>
         <!-- Scripts -->
        <script src="/js/app.js"></script>
    </body>

</html>

